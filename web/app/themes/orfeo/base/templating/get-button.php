<?php

/**
 * Get theme button base on ACF Link field
 *
 * @param array ACF Link field
 * @param string Gutenberg style name
 * @param string additional custom class
 */
if ( ! function_exists( 'get_button' ) ) {

	function get_button( $link, $style = '', $custom_class = '', $icon = '' ) {
		$link_html = '';

		if ( ! empty( $link ) && is_array( $link ) ) {
			$link_url    = $link['url'];
			$link_title  = $link['title'];
			$link_target = $link['target'] ? $link['target'] : '_self';
			$screen_reader_text = ! empty( $link['screen_reader_text'] ) ? $link['screen_reader_text'] : '';

			$args = [
				'link_url'           => $link_url,
				'link_title'         => $link_title,
				'link_target'        => $link_target,
				'style'              => $style,
				'set_class'          => $custom_class,
				'icon'	             => $icon,
				'screen_reader_text' => $screen_reader_text
			];

			$link_html = get_part( 'components/button/index', $args );
		}

		return $link_html;
	}

}