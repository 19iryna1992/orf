<?php
/**
 * Default Page template
 *
 * @package WordPress
 * @subpackage orfeo
 * @since orfeo 1.0
 */
get_header();
the_post();

?>
<main id="page-content" role="main" class="page-content page-content--default">
	<h1 class="page-title screen-reader-text"><?php the_title(); ?></h1>
	<div id="content" tabindex="-1" class="page-content__wrapper">

	<?php
		if ( have_rows ( 'modules' ) ) :
			$index = 1;

			while ( have_rows ( 'modules' ) ) : the_row();
				$module_name = get_row_layout();
				$module_class = str_replace( '_', '-', $module_name );
				$style_bg_color = get_sub_field( 'section_bg_style' );

				$style_bg_color = $style_bg_color ? "white" : "black";

				$class = ! empty( $style_bg_color ) ? "$module_class style-bg--$style_bg_color" : $module_class;
				?>

				<section id="section-<?php echo unique_ID( 'module' ) ?>" class="<?php echo $class ?><?php echo $module_name == 'hero' ? ' loading' : ''; ?>" data-color="<?php echo $style_bg_color ?>">
					<?php echo get_part( 'modules/' . $module_class . '/index', [ 'class' => $module_name, 'section_bg_style' => $style_bg_color ] ); ?>
				</section>

				<?php
				$index++;
			endwhile;

		endif;
		?>
	</div>
</main>
<?php
get_footer();
