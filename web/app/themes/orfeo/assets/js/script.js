import animateTextRevealUp from './modules/animateTextRevealUp';
import animateTextRevealDown from './modules/animateTextRevealDown';
import initCirclesAnimation from './modules/animateCircles';


document.body.addEventListener('mousedown', () => {
	document.body.classList.add('using-mouse');
});

document.body.addEventListener('keydown', event => {
	if (event.key === 'Tab') {
		document.body.classList.remove('using-mouse');
	}
});

window.addEventListener('load', () => {
	document.querySelector('body').classList.add('page-has-loaded');
});

const mobileMenuHeight = () => {
	const r = document.querySelector('.main-header__mobile');
	r.style.setProperty('--height', window.innerHeight + 'px');
	window.addEventListener('resize', () => {
		r.style.setProperty('--height', window.innerHeight + 'px');
	});
};

mobileMenuHeight();

animateTextRevealUp();
animateTextRevealDown();
initCirclesAnimation();
