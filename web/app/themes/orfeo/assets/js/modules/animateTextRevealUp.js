'use-strict';

import { gsap } from '../plugins/gsap.min.js';
import { ScrollTrigger } from '../plugins/scroll-trigger.min.js';

// Select text-revealup-animate elements
const animatedTextUpSelector = '.text-revealup-animate';
const parentDivClass = 'parentline';
const textRevealUpElements = document.querySelectorAll(animatedTextUpSelector);

// Wrap each line of text in a span element
const wrapWordsInSpan = () => {
	// Split text content by line and create a document fragment to hold the new elements
	textRevealUpElements.forEach(element => {
		element.innerHTML = element.innerHTML
			.split('<br>')
			.map(part => `
			  <div class="${parentDivClass}">
				<div>${part}</div>
			  </div>
			`)
			.join('');
	});
};

// Animate the text using gsap and scroll-trigger
const animateTextRevealUp = () => {
	if (textRevealUpElements.length) {
		gsap.registerPlugin(ScrollTrigger);
		wrapWordsInSpan();

		const elementsToAnimate = document.querySelectorAll(`${animatedTextUpSelector} .${parentDivClass} div`);

		elementsToAnimate.forEach(element => {
			const tl = gsap.timeline({
				scrollTrigger: {
					trigger: element,
					start: 'top 85%',
					end: 'bottom 100%',
					once: false,
					// markers: true,// use for debugging
					onEnterBack: () => tl.reverse(),
				},
			});

			tl.from(element, { opacity: 0, y: 100, duration: 0.5 });
		});
	}
};

export default animateTextRevealUp;
