'use-strict';

const manageScrollClass = circlesOnScroll => {
	for (let i = 0; i < circlesOnScroll.length; i++) {
		const element = circlesOnScroll[i];
		const circleSmaller = element.querySelector('.rounded-circle-animate--smaller');
		const circleBigger = element.querySelector('.rounded-circle-animate--bigger');

		const scrollPosition = window.scrollY;
		const rotationAngle = (scrollPosition / document.body.scrollHeight) * 600;

		circleSmaller.style.transform = `rotate(-${rotationAngle * 1.2}deg)`;
		circleBigger.style.transform = `rotate(${rotationAngle}deg)`;
	}
};

const initCirclesAnimation = () => {
	const circlesOnScroll = document.querySelectorAll('.rounded-circles-animate-scroll');

	window.addEventListener('scroll', () => {
		manageScrollClass(circlesOnScroll, 1);
	});
};

export default initCirclesAnimation;
