'use-strict';

import { gsap } from '../plugins/gsap.min.js';
import { ScrollTrigger } from '../plugins/scroll-trigger.min.js';

// Select text-revealup-animate elements
const animatedTextDownSelector = '.text-revealdown-animate';
const parentDivClass = 'parentline';
const textRevealDownElements = document.querySelectorAll(animatedTextDownSelector);

// Wrap each line of text in a span element
const wrapWordsInSpan = () => {
	// Split text content by line and create a document fragment to hold the new elements
	textRevealDownElements.forEach(element => {
		element.innerHTML = element.innerHTML
			.split('<br>')
			.map(part => `
			  <div class="${parentDivClass}">
				<div>${part}</div>
			  </div>
			`)
			.join('');
	});
};

// Animate the text using gsap and scroll-trigger
const animateTextRevealDown = () => {
	if (textRevealDownElements.length) {
		gsap.registerPlugin(ScrollTrigger);
		wrapWordsInSpan();

		const elementsToAnimate = document.querySelectorAll(`${animatedTextDownSelector} .${parentDivClass} div`);

		elementsToAnimate.forEach(element => {
			const tl = gsap.timeline({
				scrollTrigger: {
					trigger: element,
					start: 'top 85%',
					end: 'bottom 100%',
					once: false,
					// markers: true,// use for debugging
					onEnterBack: () => tl.reverse(),
				},
			});

			tl.from(element, { opacity: 0, y: -100, duration: 1 });
		});
	}
};

export default animateTextRevealDown;
