const { __ } = wp.i18n;
const { registerBlockStyle } = wp.blocks;

const styles = [
	{
		name: 'uppercase',
		label: __('Uppercase', 'orfeo'),
	},
	{
		name: 'subheading',
		label: __('Subheading', 'orfeo'),
	},
	{
		name: 'leadparagraph',
		label: __('Leadparagraph', 'orfeo'),
	},
];

wp.domReady(() => {
	styles.forEach(style => {
		registerBlockStyle('core/paragraph', style);
	});
});
