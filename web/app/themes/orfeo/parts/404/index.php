<?php
$content = get_field( 'page_404_content', 'options' );

load_inline_styles( __DIR__, 'parts/404' ) ;

if ( ! empty( $content ) ) :
	echo $content;
else :
	$home_url = get_bloginfo( 'url' );
?>
	<h1 class="page_404__title h3"><?php _e( '404 - Page not found', 'orfeo' ); ?></h1>
	<p class="page_404__description text--large has-text-uppercase">
		<a href="<?php echo $home_url; ?>" aria-label="<?php echo __( 'Back to Homepage', 'orfeo' ); ?>">
			<?php echo __( 'Take me back to Homepage ', 'orfeo' ); ?>
		</a>
	</p>
<?php
endif;
