<?php
$class = !empty($args['class']) ? $args['class'] : '';

$style_alignment = get_sub_field('section_alignment_style');
$style_alignment = !empty($style_alignment) ? "aligncenter" : "";


$subtitle    = get_sub_field('section_subtitle');
$title       = get_sub_field('section_title');
$description = get_sub_field('section_description');
$button      = get_sub_field('section_button');

$image_boxes = get_sub_field('image_boxes');

$rounded_corner = get_sub_field('rounded_corner');

echo load_inline_styles(__DIR__, $class);

?>
<div class="container">
	<?php if (!empty($title)) : ?>
		<div class="image-boxes__intro <?php echo $style_alignment; ?>">
			<h2 class="image-boxes__title h1"><?php echo $title; ?></h2>
			<?php if (!empty($description)) : ?>
				<div class="image-boxes__description text--large"><?php echo $description; ?></div>
			<?php endif; ?>
		</div>
	<?php endif; ?>

	<?php if (have_rows('image_boxes')) {
	?>
		<div class="image-boxes__list<?php echo count($image_boxes) >= 4 ? " image-boxes__list--big" : ""; ?>">
			<?php
			while (have_rows('image_boxes')) : the_row();
				$image_id    = get_sub_field('section_image');
				$image_size  = 'thumbnail-circle-small';
				$box_image   = wp_get_attachment_image($image_id, $image_size);
				$box_image   = !empty($box_image) ? $box_image : '';

				$box_title   = get_sub_field('section_title');
				$box_desc    = get_sub_field('section_description');
				$box_link    = get_sub_field('section_button');

				if ($box_link) :
					$box_link_url    = $box_link['url'];
					$box_link_title  = $box_link['title'];
					$box_link_target = $box_link['target'] ? $box_link['target'] : '_self';
				endif;

			?>
				<div class="image-boxes__col image-box">
					<?php if (!empty($box_image)) { ?>
						<div class="image-box__image"><?php echo $box_image; ?></div>
					<?php } ?>
					<?php if (!empty($box_title)) { ?>
						<h3 class="image-box__title"><?php echo $box_title; ?></h3>
					<?php } ?>
					<?php if (!empty($box_desc)) { ?>
						<div class="image-box__desc"><?php echo $box_desc; ?></div>
					<?php } ?>
					<?php if (!empty($box_link)) { ?>
						<div class="image-box__link-wrapper">
							<a class="image-box__link" href="<?php echo esc_url($box_link_url); ?>" target="<?php echo esc_attr($box_link_target); ?>">
								<?php
								echo esc_html($box_link_title);

								if (!empty($box_title)) :
									echo sprintf('<span class="screen-reader-text">%s %s</span>', __('about ', 'orfeo'), $box_title);
								endif;
								?>
							</a>
						</div>
					<?php
					}
					?>
				</div>
			<?php
			endwhile;; ?>
		</div>
	<?php } ?>
	<?php if (!empty($button)) {
	?>
		<div class="image-boxes__btn <?php echo $style_alignment; ?>">
			<?php get_button($button, '', '', 'icon-dot'); ?>
		</div>
	<?php
	} ?>
</div>
<?php
