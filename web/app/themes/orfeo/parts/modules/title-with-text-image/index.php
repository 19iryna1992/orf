<?php
$class = ! empty ( $args['class'] ) ? $args['class'] : '';
$module_class = str_replace( '_', '-', $class );

load_blocks_script($module_class, 'orfeo/' . $module_class, '', 'parts/modules');
echo load_inline_styles( __DIR__, $class );

$subtitle    = get_sub_field( 'section_subtitle' );
$title       = get_sub_field( 'section_title' );
$description = get_sub_field( 'section_description' );
$imageId     = get_sub_field( 'section_image' );
$image       = wp_get_attachment_image( $imageId, '' );
?>
<?php if ( ! empty( $title ) || ! empty( $image ) ) : ?>
<div class="title-with-text-image__container container">
	<div class="title-with-text-image__row">
		<div class="title-with-text-image__text">
			<button class="title-with-text-image__button button-back">
				<?php echo get_img( 'arrow-left', '' ) ?>
			</button>
			<?php if ( ! empty( $subtitle ) ) : ?>
			<h3 class="title-with-text-image__subtitle section-subtitle"><?php echo $subtitle; ?></h3>
			<?php endif; ?>
			<?php if ( ! empty( $title ) ) : ?>
			<h2 class="title-with-text-image__title h1"><?php echo $title; ?></h2>
			<?php endif; ?>
			<?php if ( ! empty( $description ) ) : ?>
			<div class="title-with-text-image__description"><?php echo $description; ?></div>
			<?php endif; ?>
		</div>
		<?php if ( ! empty( $image ) ) : ?>
			<div class="title-with-text-image__image">
				<?php echo $image; ?>
			</div>
		<?php endif; ?>
	</div>
</div>
<?php endif; ?>
