const scriptInit = () => {
	const buttons = document.querySelectorAll('.button-back');

	for (let index = 0; index < buttons.length; index++) {
		const button = buttons[index];

		button.addEventListener('click', () => {
			if (window.history.length > 1 && document.referrer.indexOf(window.location.host) !== -1) {
				history.back();
			} else {
				window.location = '/';
			}
		});
	}

	const sections = document.querySelectorAll('.title-with-text-image');

	for (let index = 0; index < sections.length; index++) {
		const section = sections[index];

		const image = section.querySelector('.title-with-text-image__image');

		const observer = new IntersectionObserver(entries => {
			entries.forEach(entry => {
				if (entry.isIntersecting) {
					entry.target.classList.add('animate');
				}
			});
		}, { threshold: 0 });

		observer.observe(image);
	}
};

scriptInit();