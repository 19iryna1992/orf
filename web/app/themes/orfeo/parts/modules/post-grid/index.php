<?php
$class = !empty($args['class']) ? $args['class'] : '';
$module_class = str_replace('_', '-', $class);

load_inline_styles(__DIR__, $class);
load_blocks_script($module_class, 'orfeo/' . $module_class, '', 'parts/modules');

$section_title = get_sub_field('section_title');
$count = 1;

$posts_args = [
    'post_type' => 'project',
    'post_status' => 'publish',
    'posts_per_page' => 8,
];

$posts_query = new WP_Query($posts_args);
$posts_query_posts = $posts_query->posts;
$post_not_in = [];

foreach( $posts_query_posts as $posts_query_post) {
    array_push($post_not_in, $posts_query_post->ID);
}

$more_query = new WP_Query( [
	'post_type'      => 'project',
	'post_status'    => 'publish',
    'posts_per_page' => 1,
	'post__not_in'   => $post_not_in
] );

?>

<?php if ($posts_query->have_posts()) : ?>

    <div class="container">
        <?php if (!empty($section_title)) : ?>
            <div class="post-grid__row row">
                <h2 class="post-grid__section-title col-12"><?php echo $section_title; ?></h2>
            </div>
        <?php endif; ?>
        <?php echo get_template_part('/parts/modules/post-grid/filters');
        ?>
        <div class="post-grid__row post-grid__row--grid row js--post-grid" data-exclude="<?php echo implode(',', $post_not_in); ?>" data-exclude-origin="<?php echo implode(',', $post_not_in); ?>">
            <?php
            while ($posts_query->have_posts()) :
                $posts_query->the_post();

                $project_id = get_the_ID();
                $project_cat = get_the_terms($project_id, 'project-category');
                $project_type = get_the_terms($project_id, 'project-type');
                $precise_location = get_field('precise_location', $project_id);
            ?>
                <div class="post-grid__articles <?php echo $count % 2 == 0 ? 'post-grid__articles--right' : 'post-grid__articles--left' ?> col-12 col-lg-6">
                    <?php if ( !empty ( $precise_location ) ) : ?>
                        <div class="post-grid__articles-location">
                            <span class=""><?php echo $precise_location; ?></span>
                        </div>
                    <?php endif; ?>
                    <a class="post-grid__articles-thumbnail" href="<?php echo get_permalink(); ?>">
                        <?php echo get_the_post_thumbnail($project_id, array('700', '500')); ?>
                    </a>
                    <div class="post-grid__articles-info">
                        <div class="post-grid__articles-title">
                            <?php echo get_the_title(); ?>
                        </div>
                        <span class="post-grid__articles-details">
                            <?php if ($project_cat && !is_wp_error($project_cat)) : ?>
                                <?php foreach ($project_cat as $term) : ?>
                                    <span class="post-grid__" >
                                        <?php echo $term->name ?>
                                    </span>
                                <?php endforeach ?>
                            <?php endif; ?>
                            <?php if ($project_cat && !is_wp_error($project_cat) && $project_type && !is_wp_error($project_type)) : ?>
                            -
                            <?php endif; ?>
                            <?php if ($project_type && !is_wp_error($project_type)) : ?>
                                <?php foreach ($project_type as $term) : ?>
                                    <span class="post-grid__">
                                        <?php echo $term->name ?>
                                    </span>
                                <?php endforeach ?>
                            <?php endif; ?>
                        </span>
                    </div>
                    <div class="post-grid__articles-permalink">
                        <?php echo "<a href='" . get_permalink() . "'>" . __('En savoir ', 'orfeo') . "</a>"; ?>
                    </div>
                </div>
            <?php
                ++$count;
            endwhile; ?>
        </div>
        <div class="post-grid__row">
            <div class="post-grid__footer">

            </div>
        </div>
    </div>

<?php endif;
wp_reset_postdata();
?>

<?php if ( $more_query->have_posts() ) : ?>
    <div class="post-grid__more">
        <div class="wp-block-button wp-block-button--template">
            <button class="wp-block-button__link">
                <svg width="12" height="12" viewBox="0 0 12 12" fill="none" xmlns="http://www.w3.org/2000/svg">
                    <path d="M5.63211 9.60354C7.60884 9.60354 9.24043 7.98986 9.24043 6.01576C9.24043 4.04166 7.60884 2.3968 5.63211 2.3968C3.65538 2.3968 2.03945 4.04178 2.03945 6.01576C2.03945 7.98973 3.65551 9.60354 5.63211 9.60354ZM5.63211 0.360107C8.76984 0.360107 11.28 2.88241 11.28 6.01576C11.28 9.1491 8.76984 11.6401 5.63211 11.6401C2.49438 11.6401 0 9.14898 0 6.01576C0 2.88253 2.49438 0.360107 5.63211 0.360107Z" fill="currentColor"></path>
                </svg><?php _e('Voir Plus', 'orfeo'); ?>
            </button>
        </div>
    </div>
<?php endif; ?>