<?php

$taxonomies = get_sub_field('filters');

if (!empty($taxonomies) && is_array($taxonomies)) : ?>
    <div class="post-grid__row post-grid__row--filter row">
        <div class="col-12">
            <div class="post-grid__filter--mobile">
                <button type="button" class="post-grid__filter-btn js--open-filter-btn">
                    <?php
                    echo get_img('filter', 'full');
                    echo __('Filtres', 'orfeo');
                    ?>
                </button>
            </div>
            <div class="post-grid__filter">
                <span><?php echo __('Filtres:', 'orfeo') ?></span>

                <?php foreach ($taxonomies as $taxonomy) :

                    if ($taxonomy == 'project_type') :
                        $terms = get_terms('project-type');
                        if (!empty($terms) && !is_wp_error($terms)) : ?>
                        
                            <span><?php echo __('Typologie', 'orfeo'); ?></span>

                        <?php endif; ?>
                        <?php elseif ($taxonomy == 'project_mission') :
                        $terms = get_terms('project-category');
                        if (!empty($terms) && !is_wp_error($terms)) :
                        ?>

                            <span><?php echo __('Mission', 'orfeo'); ?></span>

                        <?php endif; ?>

                        <?php elseif ($taxonomy == 'project_location') :
                        $terms = get_terms('project-location');
                        if (!empty($terms) && !is_wp_error($terms)) :
                        ?>

                            <span class="post-grid__filter--localisation"><?php echo __('Localisation', 'orfeo'); ?></span>
                        <?php endif; ?>
                <?php endif;
                endforeach;
                ?>

                <a href="javascript:void(0)" class="js--open-filter post-grid__open-filter">
                    <?php echo get_img('arrow_down_white', 'full') ?>
                </a>
            </div>
        </div>
        <div class="col-12">
            <div class="post-grid__filter-wrap js--filter-items">
                <div class="post-grid__filter-items">

                    <?php foreach ($taxonomies as $taxonomy) :

                        if ($taxonomy == 'project_type') :
                            $terms_type = get_terms('project-type');
                            if (!empty($terms_type) && !is_wp_error($terms_type)) :
                    ?>
                                <ul class="post-grid__filter-list">
                                    <li class="post-grid__filter-title">
                                        <span><?php echo __('Typologie', 'orfeo'); ?></span>
                                    </li>
                                    <?php foreach ($terms_type as $term) : ?>
                                        <li class="post-grid__filter-item js--filter-item" data-taxonomy="project-type" data-term-id="<?php echo $term->term_id ?>">
                                            <a href="javascript:void(0)"><?php echo $term->name ?></a>
                                        </li>

                                    <?php endforeach; ?>
                                </ul>
                            <?php endif; ?>

                            <?php elseif ($taxonomy == 'project_mission') :
                            $terms_cat = get_terms('project-category');
                            if (!empty($terms_cat) && !is_wp_error($terms_cat)) :

                            ?>
                                <ul class="post-grid__filter-list">
                                    <li class="post-grid__filter-title">
                                        <span><?php echo __('Mission', 'orfeo'); ?></span>
                                    </li>

                                    <?php foreach ($terms_cat as $term) : ?>
                                        <li class="post-grid__filter-item js--filter-item" data-taxonomy="project-category" data-term-id="<?php echo $term->term_id ?>">
                                            <a href="javascript:void(0)"><?php echo $term->name ?></a>
                                        </li>
                                    <?php endforeach; ?>
                                </ul>
                            <?php endif; ?>

                            <?php elseif ($taxonomy == 'project_location') :
                            $terms_location = get_terms('project-location');

                            if (!empty($terms_location) && !is_wp_error($terms_location)) :
                            ?>
                                <ul class="post-grid__filter-list post-grid__filter-list--localisation">
                                    <li class="post-grid__filter-title">
                                        <span><?php echo __('Localisation', 'orfeo'); ?></span>
                                    </li>

                                    <?php foreach ($terms_location as $term) : ?>
                                        <li class="post-grid__filter-item js--filter-item" data-taxonomy="project-location" data-term-id="<?php echo $term->term_id ?>">
                                            <a href="javascript:void(0)"><?php echo $term->name ?></a>
                                        </li>
                                    <?php endforeach; ?>
                                </ul>
                            <?php endif; ?>
                    <?php endif;
                    endforeach;
                    ?>
                </div>
            </div>
        </div>
        <div class="col-12">
            <ul class="post-grid__filter-terms js--filter-selected"></ul>
        </div>
    </div>
<?php endif; ?>