/* global vars */

const filters = document.querySelector('.post-grid__row--filter');
const grid = document.querySelector('.post-grid__row--grid');
const openFilter = document.querySelector('.js--open-filter');
const openFilterBtn = document.querySelector('.js--open-filter-btn');
const filterItems = document.querySelector('.js--filter-items');
const filterItem = document.querySelectorAll('.js--filter-item');
const postGrid = document.querySelector('.js--post-grid');
const filtetSelected = document.querySelector('.js--filter-selected');
const section = document.querySelector('.post-grid');
const moreButton = document.querySelector('.post-grid__more .wp-block-button__link');
const closeSvg = `<svg width="9" height="10" viewBox="0 0 9 10" fill="none" xmlns="http://www.w3.org/2000/svg">
<path fill-rule="evenodd" clip-rule="evenodd" d="M8.78239 1.53239C8.83584 1.47901 8.87825 1.41562 8.9072 1.34585C8.93615 1.27608 8.95107 1.20128 8.95112 1.12574C8.95117 1.0502 8.93633 0.975393 8.90747 0.905585C8.8786 0.835777 8.83627 0.772337 8.78289 0.718889C8.72951 0.665441 8.66612 0.623031 8.59635 0.59408C8.52658 0.565129 8.45178 0.550205 8.37624 0.550158C8.3007 0.550112 8.22589 0.564945 8.15608 0.59381C8.08628 0.622675 8.02284 0.665007 7.96939 0.71839L4.50039 4.18739L1.03239 0.71839C0.924447 0.610446 0.778044 0.549805 0.62539 0.549805C0.472735 0.549805 0.326333 0.610446 0.21839 0.71839C0.110446 0.826333 0.0498047 0.972735 0.0498047 1.12539C0.0498047 1.27804 0.110446 1.42445 0.21839 1.53239L3.68739 5.00039L0.21839 8.46839C0.164942 8.52184 0.122544 8.58529 0.0936184 8.65512C0.0646925 8.72496 0.0498047 8.7998 0.0498047 8.87539C0.0498047 8.95098 0.0646925 9.02582 0.0936184 9.09566C0.122544 9.16549 0.164942 9.22894 0.21839 9.28239C0.326333 9.39033 0.472735 9.45098 0.62539 9.45098C0.700976 9.45098 0.775823 9.43609 0.845656 9.40716C0.91549 9.37823 0.978942 9.33584 1.03239 9.28239L4.50039 5.81339L7.96939 9.28239C8.07733 9.3902 8.22368 9.45071 8.37624 9.45062C8.5288 9.45053 8.67508 9.38983 8.78289 9.28189C8.8907 9.17395 8.95121 9.0276 8.95112 8.87504C8.95103 8.72248 8.89033 8.5762 8.78239 8.46839L5.31339 5.00039L8.78239 1.53239Z" fill="black"/>
</svg>`;


const location = [];
const type = [];
const cat = [];



openFilter.addEventListener('click', () => {
	openFilter.classList.toggle('open');
	filterItems.classList.toggle('open');
});

// eslint-disable-next-line id-length
openFilterBtn.addEventListener('click', e => {
	filterItems.classList.toggle('open');
	e.target.classList.toggle('active');
});

filterItem.forEach(item => {
	item.addEventListener('click', () => {
		grid.dataset.exclude = '';
		const { taxonomy } = item.dataset;
		const termId = Number(item.dataset.termId);
		const termName = item.querySelector('a').textContent;
		const formData = new FormData();

		if (item.classList.contains('selected')) {
			item.classList.remove('selected');
			if (taxonomy == 'project-type') {
				const termIndex = type.indexOf(termId);
				const removedItem = document.getElementById(`project-type-${termId}`);
				type.splice(termIndex, 1);
				filtetSelected.removeChild(removedItem);
			} else if (taxonomy == 'project-category') {
				const catIndex = cat.indexOf(termId);
				const removedItem = document.getElementById(`project-category-${termId}`);
				cat.splice(catIndex, 1);
				filtetSelected.removeChild(removedItem);
			} else if (taxonomy == 'project-location') {
				const locationIndex = location.indexOf(termId);
				const removedItem = document.getElementById(`project-location-${termId}`);
				location.splice(locationIndex, 1);
				filtetSelected.removeChild(removedItem);
			}
		} else {
			item.classList.add('selected');

			if (taxonomy == 'project-type') {
				type.push(termId);
				filtetSelected.innerHTML += `<li id='project-type-${termId}' class='post-grid__filter-term js--filter-term' data-taxonomy='project-type' data-term-id='${termId}'>${termName}<span class='js--remove-item'>${closeSvg}</span></li>`;
			} else if (taxonomy == 'project-category') {
				cat.push(termId);
				filtetSelected.innerHTML += `<li id='project-category-${termId}' class='post-grid__filter-term js--filter-term' data-taxonomy='project-category'data-term-id='${termId}'>${termName}<span class='js--remove-item'>${closeSvg}</span></li>`;
			} else if (taxonomy == 'project-location') {
				location.push(termId);
				filtetSelected.innerHTML += `<li id='project-location-${termId}' class='post-grid__filter-term js--filter-term' data-taxonomy='project-location' data-term-id='${termId}'>${termName}<span class='js--remove-item'>${closeSvg}</span></li>`;
			}
		}

		formData.append('action', 'project_filter');

		if (type.length != 0) {
			formData.append('type', type);
		}
		if (cat.length != 0) {
			formData.append('cat', cat);
		}
		if (location.length != 0) {
			formData.append('location', location);
		}

		postGrid.classList.add('loading');

		fetch(vars.ajaxUrl, {
			method: 'POST',
			body: formData,
		})
			.then(response => response.text())
			.then(data => {
				setTimeout(() => {
					const response = JSON.parse(data);
					postGrid.innerHTML = response.output;

					if (response.excludeFilter !== undefined) {
						grid.dataset.exclude = response.excludeFilter;
					}

					if (response.count !== undefined) {
						if (response.count == 0) {
							moreButton.parentNode.parentNode.classList.add('hidden');
						} else {
							moreButton.parentNode.parentNode.classList.remove('hidden');
						}
					}

					postGrid.classList.remove('loading');
				}, 300);
			})
			.catch(error => {
				console.error(error);

				postGrid.classList.remove('loading');
			});

		const selectedItems = document.querySelectorAll('.post-grid__filter-item');
		let numbers = '';
		let selected = 0;
		selectedItems.forEach(item => {
			let dividing = ',';

			if (item.classList.contains('selected')) {
				selected = selected + 1;
				if (numbers == '') {
					dividing = '';
				}
				numbers = numbers + dividing + item.dataset.termId;
			}
		});
		grid.dataset.terms = numbers;
		grid.dataset.offset = 0;
	});
});

document.addEventListener('click', e => {
	const target = e.target.closest('.js--remove-item');

	if (target) {
		grid.dataset.exclude = '';

		const { taxonomy } = target.parentElement.dataset;
		const termId = Number(target.parentElement.dataset.termId);
		const formData = new FormData();


		filterItem.forEach(item => {
			const itemTaxonomy = item.dataset.taxonomy;
			const itemTermId = item.dataset.termId;

			if (taxonomy == itemTaxonomy && termId == itemTermId && item.classList.contains('selected')) {
				item.classList.remove('selected');
				if (taxonomy == 'project-type') {
					const termIndex = type.indexOf(termId);
					const removedItem = document.getElementById(`project-type-${termId}`);
					type.splice(termIndex, 1);
					filtetSelected.removeChild(removedItem);
				} else if (taxonomy == 'project-category') {
					const catIndex = cat.indexOf(termId);
					const removedItem = document.getElementById(`project-category-${termId}`);
					cat.splice(catIndex, 1);
					filtetSelected.removeChild(removedItem);
				} else if (taxonomy == 'project-location') {
					const locationIndex = location.indexOf(termId);
					const removedItem = document.getElementById(`project-location-${termId}`);
					location.splice(locationIndex, 1);
					filtetSelected.removeChild(removedItem);
				}

				formData.append('action', 'project_filter');

				if (type.length != 0) {
					formData.append('type', type);
				}
				if (cat.length != 0) {
					formData.append('cat', cat);
				}
				if (location.length != 0) {
					formData.append('location', location);
				}

				postGrid.classList.add('loading');

				fetch(vars.ajaxUrl, {
					method: 'POST',
					body: formData,
				})
					.then(response => response.text())
					.then(data => {
						setTimeout(() => {
							// postGrid.innerHTML = data;
							const response = JSON.parse(data);
							postGrid.innerHTML = response.output;

							if (response.excludeFilter !== undefined) {
								grid.dataset.exclude = response.excludeFilter;
							}

							if (response.count !== undefined) {
								if (response.count == 0) {
									moreButton.parentNode.parentNode.classList.add('hidden');
								} else {
									moreButton.parentNode.parentNode.classList.remove('hidden');
								}
							}

							postGrid.classList.remove('loading');
						}, 300);
					})
					.catch(error => {
						console.error(error);

						postGrid.classList.remove('loading');
					});

				const selectedItems = document.querySelectorAll('.post-grid__filter-item');
				let numbers = '';
				let selected = 0;
				selectedItems.forEach(item => {
					let dividing = ',';

					if (item.classList.contains('selected')) {
						selected = selected + 1;
						if (numbers == '') {
							dividing = '';
						}
						numbers = numbers + dividing + item.dataset.termId;
					}
				});
				grid.dataset.terms = numbers;
				grid.dataset.offset = 0;
			}
		});
	}
});

const projectShowMore = (section, emptyContent = 0) => {
	const contentEl    = section.querySelector('.post-grid__row--grid');
	const buttonEl     = section.querySelector('.post-grid__more .wp-block-button__link');
	const offset       = contentEl.dataset.offset;
	const termId       = contentEl.dataset.terms;
	const exclude      = contentEl.dataset.exclude;
	const perPage      = contentEl.dataset.perPage;
	const data         = new FormData();

	contentEl.classList.add('loading');

	data.append('action', 'project_show_more');
	data.append('empty_content', emptyContent);

	// if (taxonomy !== undefined) {
	// 	data.append('taxonomy', taxonomy);
	// }
	if (offset !== undefined) {
		data.append('offset', offset);
	}
	if (termId !== undefined) {
		data.append('term_ID', termId);
	}
	if (exclude !== undefined) {
		data.append('exclude', exclude);
	}

	if (type.length != 0) {
		data.append('type', type);
	}
	if (cat.length != 0) {
		data.append('cat', cat);
	}
	if (location.length != 0) {
		data.append('location', location);
	}
	if (perPage !== undefined && emptyContent === 1) {
		data.append('per_page', perPage);
	} else {
		data.append('per_page', 8);
	}

	fetch(vars.ajaxUrl, {
		method: 'POST',
		body: data,
	})
		.then(response => {
			return response.text();
		})
		.then(data => {
			setTimeout(() => {
				try {
					const response = JSON.parse(data);
					if (response.output !== undefined) {
						if (emptyContent === 1) {
							contentEl.innerHTML = '';
						}

						contentEl.innerHTML += response.output;
					}

					if (response.offset !== undefined) {
						if (response.offset === 7) {
							contentEl.dataset.offset = 0;
						} else {
							contentEl.dataset.offset = response.offset;
						}
					}

					if (response.count !== undefined) {
						if (response.count == 0) {
							buttonEl.parentNode.parentNode.classList.add('hidden');
						} else {
							buttonEl.parentNode.parentNode.classList.remove('hidden');
						}
					}
					contentEl.classList.remove('loading');
				} catch (error) {
					console.error('Error: ', data);

					contentEl.classList.remove('loading');
				}
				contentEl.classList.remove('loading');
			}, 300);
		})
		.catch(error => {
			console.error('Error: ', error.message);

			contentEl.classList.remove('loading');
		});
};

if (moreButton !== null) {
	moreButton.addEventListener('click', () => {
		projectShowMore(section);
	});
}

