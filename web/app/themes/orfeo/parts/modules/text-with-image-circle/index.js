const scriptInit = () => {
	const sections = document.querySelectorAll('.text-with-image-circle');

	for (let index = 0; index < sections.length; index++) {
		const element = sections[index];
		const circle = element.querySelector('.text-with-image-circle__circle');

		const observer = new IntersectionObserver(entries => {
			entries.forEach(entry => {
				if (entry.isIntersecting) {
					entry.target.classList.add('pulse');
				} else {
					entry.target.classList.remove('pulse');
				}
			});
		}, { threshold: 0 });

		observer.observe(circle);
	}
};

scriptInit();
