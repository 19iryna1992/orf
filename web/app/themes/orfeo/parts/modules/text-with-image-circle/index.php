<?php
$class = ! empty ( $args['class'] ) ? $args['class'] : '';
$module_class = str_replace( '_', '-', $class );

$image_id    = get_sub_field( 'section_image');
$image_size  = 'thumbnail-img-with-circle';
$image       = wp_get_attachment_image( $image_id, $image_size );
$image       = ! empty( $image ) ? $image : '';

$subtitle    = get_sub_field( 'section_subtitle' );
$title       = get_sub_field( 'section_title' );
$list_btns   = get_sub_field( 'section_list_buttons' );

$circle_image = get_img( 'orange-circle', 'full' );
$circle_image = ! empty( $circle_image ) ? $circle_image : '';

echo load_inline_styles( __DIR__, $class );
load_blocks_script($module_class, 'orfeo/' . $module_class, '', 'parts/modules');

?>
	<div class="container">
		<div class="row">
			<div class="text-with-image-circle__col text-with-image-circle__col--image col-12 col-xl-6">
				<div class="text-with-image-circle__image-wrapper">
					<?php if( $circle_image ) { ?>
						<div class="text-with-image-circle__circle">
							<?php echo $circle_image; ?>
						</div>
					<?php } ?>
					<?php echo $image; ?>
				</div>
			</div>
			<div class="text-with-image-circle__col text-with-image-circle__col--content col-12 col-xl-6 col-xxl-5">
				<?php if ( ! empty( $subtitle ) ) { ?>
					<h3 class="text-with-image-circle__subtitle section-subtitle"><?php echo $subtitle; ?></h3>
				<?php } ?>
				<?php if ( ! empty( $title ) ) { ?>
					<h2 class="text-with-image-circle__title h1"><?php echo $title; ?></h2>
				<?php } ?>
				<?php
					if ( have_rows( 'section_list_buttons' ) ) {
						?>
						<div class="text-with-image-circle__buttons section_list_buttons">
						<?php
							while ( have_rows( 'section_list_buttons' ) ) : the_row();
								$button = get_sub_field( 'section_button' );

								if( ! empty( $button ) ) :
									get_button( $button , '', '', 'icon-dot' );
								endif;

							endwhile;
							?>
						</div>
						<?php
					}
				?>
			</div>
		</div>
	</div>
<?php
