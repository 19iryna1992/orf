const sliderInit = () => {
	const sections = document.querySelectorAll('.portfolio-slider');

	for (let index = 0; index < sections.length; index++) {
		const element = sections[index];
		const slider = element.querySelector('.portfolio-slider__slider');

		const splide = new Splide(slider, {
			perPage: 1,
			perMove: 1,
			gap: '50px',
			arrows: true,
			pagination: false,
			focus: 'center',
		});
		splide.mount();
	}
};

sliderInit();
