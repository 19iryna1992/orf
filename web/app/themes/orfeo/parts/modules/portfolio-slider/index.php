<?php
$class = ! empty ( $args['class'] ) ? $args['class'] : '';
$module_class = str_replace( '_', '-', $class );

$section_title = get_sub_field( 'section_title' );
$portfolio = get_sub_field( 'portfolio' );
$link = get_sub_field( 'link' );

load_inline_styles_plugin('splide.min');
load_inline_styles_shared( 'sliders' );
load_inline_styles( __DIR__, $class );

echo load_blocks_script( $module_class, 'splide/' . $module_class, ['splide'], 'parts/modules' ); ?>

<div class="container">
	<?php if ( ! empty( $portfolio ) ) : ?>
	<div class="portfolio-slider__row">
		<?php if ( ! empty( $section_title ) ) : ?>
		<h2 class="portfolio-slider__title h1">
			<?php echo $section_title; ?>
		</h2>
		<?php endif; ?>
		<div class="portfolio-slider__slider splide">
			<div class="splide__track">
				<ul class="splide__list">
				<?php foreach( $portfolio as $post ):
				setup_postdata($post);
				$title       = get_the_title( $post );
				$post_link   = get_permalink( $post );
				$image       = get_the_post_thumbnail( $post );
				$description = get_field( 'small_description', $post );
				?>
					<li class="portfolio-slider__item splide__slide">
						<a class="portfolio-slider__item__link" href="<?php echo $post_link; ?>">
							<div class="portfolio-slider__item__image"><?php echo $image; ?></div>
							<h2 class="portfolio-slider__item__title h3"><?php echo $title; ?></h2>
							<?php if ( !empty( $description ) ) : ?>
							<div class="portfolio-slider__item__description"><?php echo $description; ?></div>
							<?php endif; ?>
						</a>
					</li>
				<?php endforeach;
				wp_reset_postdata();
				?>
				</ul>
			</div>
			<div class="splide__arrows">
				<button class="splide__arrow splide__arrow--prev">
					<svg width="41" height="27" viewBox="0 0 41 27" fill="none" xmlns="http://www.w3.org/2000/svg">
						<path fill-rule="evenodd" clip-rule="evenodd" d="M40.4043 13.5C40.4043 13.1187 40.2528 12.7531 39.9833 12.4835C39.7137 12.2139 39.348 12.0625 38.9668 12.0625H5.06192L14.1095 3.01774C14.3795 2.74781 14.5311 2.38172 14.5311 1.99998C14.5311 1.61825 14.3795 1.25216 14.1095 0.982234C13.8396 0.71231 13.4735 0.560669 13.0918 0.560669C12.7101 0.560669 12.344 0.71231 12.074 0.982234L0.574051 12.4822C0.440182 12.6158 0.333969 12.7744 0.261501 12.949C0.189034 13.1237 0.15173 13.3109 0.15173 13.5C0.15173 13.6891 0.189034 13.8763 0.261501 14.0509C0.333969 14.2256 0.440182 14.3842 0.574051 14.5177L12.074 26.0177C12.344 26.2877 12.7101 26.4393 13.0918 26.4393C13.4735 26.4393 13.8396 26.2877 14.1095 26.0177C14.3795 25.7478 14.5311 25.3817 14.5311 25C14.5311 24.6183 14.3795 24.2522 14.1095 23.9822L5.06192 14.9375H38.9668C39.348 14.9375 39.7137 14.786 39.9833 14.5165C40.2528 14.2469 40.4043 13.8812 40.4043 13.5Z" fill="currentColor"/>
					</svg>
				</button>
				<button class="splide__arrow splide__arrow--next">
					<svg width="41" height="27" viewBox="0 0 41 27" fill="none" xmlns="http://www.w3.org/2000/svg">
						<path fill-rule="evenodd" clip-rule="evenodd" d="M0.154297 13.5C0.154297 13.1187 0.305747 12.7531 0.575331 12.4835C0.844914 12.2139 1.21055 12.0625 1.5918 12.0625H35.4967L26.449 3.01774C26.1791 2.74781 26.0275 2.38172 26.0275 1.99998C26.0275 1.61825 26.1791 1.25216 26.449 0.982234C26.719 0.71231 27.0851 0.560669 27.4668 0.560669C27.8485 0.560669 28.2146 0.71231 28.4845 0.982234L39.9845 12.4822C40.1184 12.6158 40.2246 12.7744 40.2971 12.949C40.3696 13.1237 40.4069 13.3109 40.4069 13.5C40.4069 13.6891 40.3696 13.8763 40.2971 14.0509C40.2246 14.2256 40.1184 14.3842 39.9845 14.5177L28.4845 26.0177C28.2146 26.2877 27.8485 26.4393 27.4668 26.4393C27.0851 26.4393 26.719 26.2877 26.449 26.0177C26.1791 25.7478 26.0275 25.3817 26.0275 25C26.0275 24.6183 26.1791 24.2522 26.449 23.9822L35.4967 14.9375H1.5918C1.21055 14.9375 0.844914 14.786 0.575331 14.5165C0.305747 14.2469 0.154297 13.8812 0.154297 13.5Z" fill="currentColor"/>
					</svg>
				</button>
			</div>
		</div>
		<?php if ( !empty( $link ) ) : ?>
		<div class="portfolio-slider__link"><?php echo get_button( $link , '', '', 'icon-dot' ); ?></div>
		<?php endif; ?>
	</div>
	<?php endif; ?>
</div>

