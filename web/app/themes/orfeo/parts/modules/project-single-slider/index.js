const clearActiveFromButtons = buttons => {
	for (let index = 0; index < buttons.length; index++) {
		const button = buttons[index];
		button.classList.remove('active');
	}
};

const clearActiveFromTabs = tabs => {
	for (let index = 0; index < tabs.length; index++) {
		const tab = tabs[index];
		tab.classList.remove('active');
	}
};

const sliderInit = () => {
	const sections = document.querySelectorAll('.project-single-slider');

	for (let index = 0; index < sections.length; index++) {
		const element = sections[index];
		const slider = element.querySelectorAll('.project-single-slider__slider');
		const buttons = element.querySelectorAll('.project-single-slider__category');
		const tabs = element.querySelectorAll('.project-single-slider__content-tab');

		for (let index = 0; index < slider.length; index++) {
			const singleSlider = slider[index];
			const splide = new Splide(singleSlider, {
				perPage: 1,
				perMove: 1,
				gap: '25px',
				arrows: true,
				pagination: true,
				focus: 'center',
				mediaQuery: 'min',
				breakpoints: {
					768: {
						gap: '50px',
					},
				},
			});
			splide.mount();
		}

		for (let index = 0; index < buttons.length; index++) {
			const button = buttons[index];
			button.addEventListener('click', () => {
				const tab = button.dataset.tab;

				clearActiveFromButtons(buttons);
				button.classList.add('active');

				clearActiveFromTabs(tabs);
				element.querySelector(`.project-single-slider__content-tab[data-tab="${tab}"]`).classList.add('active');
			});
		}
	}
};

sliderInit();
