<?php
$class = ! empty ( $args['class'] ) ? $args['class'] : '';
$module_class = str_replace( '_', '-', $class );

echo load_inline_styles( __DIR__, $class );
load_blocks_script($module_class, 'orfeo/' . $module_class, '', 'parts/modules');

$style_bg_color = get_sub_field( 'section_bg_style' );
$style_bg_color = $style_bg_color ? "white" : "black";
$section_title  = get_sub_field( 'section_title' );
$boxes          = get_sub_field( 'boxes' );
$columns        = get_sub_field( 'columns' );
$logos          = get_sub_field( 'logos' );
?>
<?php if ( ! empty( $boxes ) ) : ?>
<div class="title-with-text-boxes__container container">
	<div class="title-with-text-boxes__row">
	<?php if ( ! empty( $section_title ) ) : ?>
		<h2 class="title-with-text-boxes__title h1"><?php echo $section_title; ?></h2>
	<?php endif; ?>
	<?php if ( ! empty( $boxes ) ) : ?>
		<ul class="title-with-text-boxes__list<?php echo $columns == '1' ? ' title-with-text-boxes__list--four' : '' ; ?>">
		<?php foreach ( $boxes as $box ) :
		$box_number      = $box['number'];
		$box_logoId      = $box['logo'];
		$box_logo        = wp_get_attachment_image( $box_logoId, 'box-logo' );
		$box_title       = $box['title'];
		$box_description = $box['description'];
		$text_uppercase  = $box['text_uppercase'];
		$box_type        = $box['type'];
		?>
			<li class="title-with-text-boxes__item">
			<?php if ( ! empty( $box_number ) && $box_type == 'number' ) : ?>
				<div class="title-with-text-boxes__item__number text-revealdown-animate">
					<div class="parentline">
						<?php echo $box_number; ?>
					</div>
				</div>
			<?php endif; ?>
			<?php if ( ! empty( $box_logo ) && $box_type == 'logo' ) : ?>
				<div class="title-with-text-boxes__item__logo text-revealdown-animate">
					<div class="parentline">
						<?php echo $box_logo; ?>
					</div>
				</div>
			<?php endif; ?>
			<?php if ( ! empty( $box_title ) && $box_type != 'title' ) : ?>
				<h3 class="title-with-text-boxes__item__title h5"><?php echo $box_title; ?></h3>
			<?php endif; ?>
			<?php if ( ! empty( $box_title ) && $box_type == 'title' ) : ?>
				<h3 class="title-with-text-boxes__item__title title-with-text-boxes__item__title--bigger h5 text-revealdown-animate">
					<div class="parentline">
						<?php echo $box_title; ?>
					</div>
				</h3>
			<?php endif; ?>
			<?php if ( ! empty( $box_description ) ) : ?>
				<div class="title-with-text-boxes__item__description<?php echo $text_uppercase == '1' ? ' title-with-text-boxes__item__description--uppercase' : '' ; ?>"><?php echo $box_description; ?></div>
			<?php endif; ?>
			</li>
		<?php endforeach; ?>
		</ul>
	<?php endif; ?>
	</div>
	<?php if ( ! empty( $logos ) ) : ?>
	<ul class="title-with-text-boxes__logos">
	<?php foreach ( $logos as $logo ) :
	$image = wp_get_attachment_image( $logo['logo'], '' );
	?>
	<li class="title-with-text-boxes__logo">
		<?php echo $image; ?>
	</li>

	<?php endforeach; ?>
	</ul>
	<?php endif; ?>
</div>
<div class="title-with-text-boxes__circles title-with-text-boxes__circles--<?php echo $style_bg_color; ?> rounded-circles-animate rounded-circles-animate-scroll">
	<div class="title-with-text-boxes__circle title-with-text-boxes__circle--smaller rounded-circle-animate rounded-circle-animate--smaller">
		<?php if ( $style_bg_color == 'black' ) : ?>
		<svg width="891" height="891" viewBox="0 0 891 891" fill="none" xmlns="http://www.w3.org/2000/svg">
			<circle cx="445.916" cy="445.916" r="444" transform="rotate(-90 445.916 445.916)" stroke="white" stroke-opacity="0.3" stroke-width="2" stroke-dasharray="18 18"/>
		</svg>
		<?php endif; ?>
		<?php if ( $style_bg_color == 'white' ) : ?>
		<svg width="891" height="892" viewBox="0 0 891 892" fill="none" xmlns="http://www.w3.org/2000/svg">
			<circle cx="445.916" cy="446" r="444" stroke="black" stroke-opacity="0.2" stroke-width="2" stroke-dasharray="18 18"/>
		</svg>
		<?php endif; ?>
	</div>
	<div class="title-with-text-boxes__circle title-with-text-boxes__circle--bigger rounded-circle-animate rounded-circle-animate--bigger">
		<?php if ( $style_bg_color == 'black' ) : ?>
		<svg width="1301" height="1301" viewBox="0 0 1301 1301" fill="none" xmlns="http://www.w3.org/2000/svg">
			<circle cx="650.916" cy="650.917" r="649" stroke="white" stroke-opacity="0.3" stroke-width="2" stroke-dasharray="18 18"/>
		</svg>
		<?php endif; ?>
		<?php if ( $style_bg_color == 'white' ) : ?>
		<svg width="1302" height="1302" viewBox="0 0 1302 1302" fill="none" xmlns="http://www.w3.org/2000/svg">
			<circle cx="650.917" cy="651" r="649" stroke="black" stroke-opacity="0.2" stroke-width="2" stroke-dasharray="18 18"/>
		</svg>
		<?php endif; ?>
	</div>
</div>
<?php endif; ?>