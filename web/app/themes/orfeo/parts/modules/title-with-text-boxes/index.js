const sectionInit = () => {
	const sections = document.querySelectorAll('.title-with-text-boxes');

	for (let i = 0; i < sections.length; i++) {
		const element = sections[i];

		const observer = new IntersectionObserver(entries => {
			entries.forEach(entry => {
				if (entry.isIntersecting) {
					entry.target.querySelector('.rounded-circle-animate--smaller').classList.add('animate');
					entry.target.querySelector('.rounded-circle-animate--bigger').classList.add('animate');
				} else {
					entry.target.querySelector('.rounded-circle-animate--smaller').classList.remove('animate');
					entry.target.querySelector('.rounded-circle-animate--bigger').classList.remove('animate');
				}
			});
		}, { threshold: 0 });

		observer.observe(element);
	}
};

sectionInit();
