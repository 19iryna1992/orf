<?php
$class = ! empty ( $args['class'] ) ? $args['class'] : '';

echo load_inline_styles( __DIR__, $class );

$section_title  = get_sub_field( 'section_title' );
$section_image  = get_sub_field( 'section_image' );
$image          = wp_get_attachment_image( $section_image, 'hero-image' );
?>
<?php if ( ! empty( $image ) ) : ?>
<div class="hero-2__image">
	<?php echo $image; ?>
</div>
<?php if ( ! empty( $section_title ) ) : ?>
<h2 class="hero-2__title h1">
	<?php echo $section_title; ?>
</h2>
<?php endif; ?>
<div class="hero-2__circles rounded-circles-animate rounded-circles-animate-scroll">
	<div class="hero-2__circle hero-2__circle--smaller rounded-circle-animate rounded-circle-animate--smaller">
		<svg width="890" height="890" viewBox="0 0 890 890" fill="none" xmlns="http://www.w3.org/2000/svg">
			<circle cx="445" cy="445" r="444" stroke="white" stroke-opacity="0.6" stroke-width="2" stroke-dasharray="17 17"/>
		</svg>
	</div>
	<div class="hero-2__circle hero-2__circle--bigger rounded-circle-animate rounded-circle-animate--bigger">
		<svg width="1300" height="1300" viewBox="0 0 1300 1300" fill="none" xmlns="http://www.w3.org/2000/svg">
			<circle cx="650" cy="650" r="649" stroke="white" stroke-opacity="0.6" stroke-width="2" stroke-dasharray="18 18"/>
		</svg>
	</div>
</div>
<?php endif; ?>