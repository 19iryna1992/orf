<?php
$class = ! empty ( $args['class'] ) ? $args['class'] : '';
$module_class = str_replace( '_', '-', $class );

load_blocks_script($module_class, 'orfeo/' . $module_class, '', 'parts/modules');
echo load_inline_styles( __DIR__, $class );

$subtitle    = get_sub_field( 'section_subtitle' );
$title       = get_sub_field( 'section_title' );
$description = get_sub_field( 'section_description' );
$list        = get_sub_field( 'list' );
$link        = get_sub_field( 'link' );

?>
<?php if ( ! empty( $list ) ) : ?>
<div class="jobs__container container">
	<div class="jobs__row">
		<?php if ( ! empty( $subtitle ) ) : ?>
		<h3 class="jobs__subtitle section-subtitle"><?php echo $subtitle; ?></h3>
		<?php endif; ?>
		<?php if ( ! empty( $title ) ) : ?>
		<h2 class="jobs__title h1"><?php echo $title; ?></h2>
		<?php endif; ?>
		<?php if ( ! empty( $description ) ) : ?>
		<div class="jobs__description"><?php echo $description; ?></div>
		<?php endif; ?>
		<?php if ( ! empty( $list ) ) : ?>
		<ul class="jobs__list">
		<?php foreach ( $list as $item ) :
		$item = $item['item'];
		?>
			<li class="jobs__item">
				<label>
					<input type="checkbox" class="jobs__checkbox"><span class="jobs__text"><?php echo $item; ?></span>
				</label>
			</li>
		<?php endforeach; ?>
		</ul>
		<?php if ( ! empty( $link ) ) : ?>
			<div class="wp-block-button wp-block-button--template  jobs__link disabled">
				<a class="wp-block-button__link" href="mailto:<?php echo $link['url']; ?>?subject=Candidature spontanée | CV %26 Lettre de motivation: Nom Prénom" title="<?php echo $link['title']; ?>" target="<?php echo $link['target']; ?>">
				<?php echo get_img( 'icon-dot' ); ?><?php echo $link['title']; ?></a>
			</div>
		<?php endif; ?>
	<?php endif; ?>
	</div>
</div>
<?php endif; ?>