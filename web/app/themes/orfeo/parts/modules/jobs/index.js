const loopCheckboxes = checkboxes => {
	let number = 0;

	for (let i = 0; i < checkboxes.length; i++) {
		const checkbox = checkboxes[i];

		if (checkbox.checked) {
			number = number + 1;
		}
	}
	return number;
};

const checkboxListInit = () => {
	const scrollTabs = document.querySelectorAll('.jobs');

	for (let i = 0; i < scrollTabs.length; i++) {
		const element = scrollTabs[i];

		const checkboxes = element.querySelectorAll('.jobs__checkbox');
		const link = element.querySelector('.jobs__link');

		for (let i = 0; i < checkboxes.length; i++) {
			const checkbox = checkboxes[i];

			checkbox.addEventListener('change', () => {
				if (loopCheckboxes(checkboxes) == checkboxes.length) {
					link.classList.remove('disabled');
				} else {
					link.classList.add('disabled');
				}
			});
		}
	}
};

checkboxListInit();
