const sliderInit = () => {
	const sections = document.querySelectorAll('.team');

	for (let index = 0; index < sections.length; index++) {
		const element = sections[index];
		const list = element.querySelector('.team__list');
		const button = element.querySelector('.team__more');

		button.querySelector('.wp-block-button__link').addEventListener('click', () => {
			list.classList.add('team__list--all');
			button.classList.add('hidden');
		});
	}
};

sliderInit();
