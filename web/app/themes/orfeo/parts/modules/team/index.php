<?php
$class = ! empty ( $args['class'] ) ? $args['class'] : '';
$module_class = str_replace( '_', '-', $class );

$subtitle         = get_sub_field( 'section_subtitle' );
$title            = get_sub_field( 'section_title' );
$description      = get_sub_field( 'section_description' );
$list_title       = get_sub_field( 'list_title' );
$team             = get_sub_field( 'team' );
$button_more_text = get_sub_field( 'button_more_text' );

load_inline_styles( __DIR__, $class );

echo load_blocks_script( $module_class, '' . $module_class, [], 'parts/modules' ); ?>

<div class="container">
	<?php if ( ! empty( $team ) ) : ?>
	<div class="team__row">
		<?php if ( !empty( $subtitle ) ) : ?>
		<h2 class="team__subtitle section-subtitle"><?php echo $subtitle; ?></h2>
		<?php endif; ?>
		<?php if ( !empty( $title ) ) : ?>
		<h2 class="team__title h1"><?php echo $title; ?></h2>
		<?php endif; ?>
		<?php if ( !empty( $description ) ) : ?>
		<div class="team__description"><?php echo $description; ?></div>
		<?php endif; ?>
		<?php if ( !empty( $list_title ) ) : ?>
		<h3 class="team__title--list h3"><?php echo $list_title; ?></h3>
		<?php endif; ?>
		<ul class="team__list">
		<?php foreach( $team as $post ):
		setup_postdata($post);
		$image_id = get_post_thumbnail_id( $post );
		$image    = wp_get_attachment_image($image_id, 'team');
		$name     = get_field( 'description', $post );
		$linkedin = get_field( 'linkedin', $post );
		?>
			<li class="team__item">
				<?php if ( !empty( $image ) ) : ?>
				<div class="team__image"><?php echo $image; ?></div>
				<?php endif; ?>
				<?php if ( !empty( $name ) ) : ?>
				<h4 class="team__name"><?php echo $name; ?></h4>
				<?php endif; ?>
				<?php if ( !empty( $linkedin ) ) : ?>
					<?php echo get_button( $linkedin , '', 'team__linkedin', '' ); ?>
				<?php endif; ?>
			</li>
			<?php endforeach;
			wp_reset_postdata();
			?>
		</ul>
		<div class="team__more">
			<div class="wp-block-button wp-block-button--template">
				<button class="wp-block-button__link">
					<svg width="12" height="12" viewBox="0 0 12 12" fill="none" xmlns="http://www.w3.org/2000/svg">
						<path d="M5.63211 9.60354C7.60884 9.60354 9.24043 7.98986 9.24043 6.01576C9.24043 4.04166 7.60884 2.3968 5.63211 2.3968C3.65538 2.3968 2.03945 4.04178 2.03945 6.01576C2.03945 7.98973 3.65551 9.60354 5.63211 9.60354ZM5.63211 0.360107C8.76984 0.360107 11.28 2.88241 11.28 6.01576C11.28 9.1491 8.76984 11.6401 5.63211 11.6401C2.49438 11.6401 0 9.14898 0 6.01576C0 2.88253 2.49438 0.360107 5.63211 0.360107Z" fill="currentColor"></path>
					</svg><?php echo ! empty ( $button_more_text ) ? $button_more_text : 'Voir tout'; ?>
				</button>
			</div>
		</div>
	</div>
	<?php endif; ?>
</div>

