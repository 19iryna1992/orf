<?php
$class = ! empty ( $args['class'] ) ? $args['class'] : '';

echo load_inline_styles( __DIR__, $class );

$list = get_sub_field( 'list' );
?>
<?php if ( ! empty( $list ) ) : ?>
<div class="roadmap__container container">
	<div class="roadmap__row">
		<ul class="roadmap__list">
		<?php foreach ( $list as $item ) :
		$item_title = $item['title'];
		$item_desc  = $item['description'];
		?>
			<li class="roadmap__item">
			<?php if ( ! empty( $item_title ) ) : ?>
				<div class="roadmap__item__title">
					<?php echo $item_title; ?>
				</div>
			<?php endif; ?>
			<?php if ( ! empty( $item_desc ) ) : ?>
				<div class="roadmap__item__description">
					<?php echo $item_desc; ?>
				</div>
			<?php endif; ?>
			</li>
		<?php endforeach; ?>
		</ul>
		<div class="roadmap__line"></div>
	</div>
</div>
<?php endif; ?>