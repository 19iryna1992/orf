const videoInit = () => {
	const sections = document.querySelectorAll('.full-screen-video');

	for (let index = 0; index < sections.length; index++) {
		const element = sections[index];
		const video = element.querySelector('.full-screen-video__video');
		const fullVideo = element.querySelector('.full-screen-video__popup__inner iframe');
		const popup = element.querySelector('.full-screen-video__popup');
		const overlay = element.querySelector('.full-screen-video__popup__overlay');
		const close = element.querySelector('.full-screen-video__popup__close');

		let number = 0;

		video.addEventListener('click', () => {
			popup.classList.add('active');
			document.getElementsByTagName('body')[0].style.overflowY = 'hidden';

			if (number === 0) {
				const iframeSrc = fullVideo.getAttribute('src');
				const srcFinished = iframeSrc.replace('&autoplay=0', '&autoplay=1');
				fullVideo.setAttribute('src', srcFinished);
			}

			number = 1;
		});

		overlay.addEventListener('click', () => {
			popup.classList.remove('active');
			document.getElementsByTagName('body')[0].style.overflowY= 'unset';
		});

		close.addEventListener('click', () => {
			popup.classList.remove('active');
			document.getElementsByTagName('body')[0].style.overflowY = 'unset';
		});
	}
};

videoInit();
