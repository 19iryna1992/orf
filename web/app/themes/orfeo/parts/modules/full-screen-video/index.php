<?php
$class = ! empty ( $args['class'] ) ? $args['class'] : '';
$module_class = str_replace( '_', '-', $class );

$preview_video = get_sub_field( 'preview_video' );
$full_video = get_sub_field( 'full_video' );

echo load_inline_styles( __DIR__, $class );
echo load_blocks_script( $module_class, '' . $module_class, [], 'parts/modules' ); ?>

<?php if ( ! empty( $preview_video ) && ! empty( $full_video ) ) : ?>
<div class="full-screen-video__video">
	<button class="full-screen-video__button">
		<?php echo get_img('play'); ?>
		<span>Jouer la vidéo</span>
	</button>
	<?php if ( preg_match('/src="(.+?)"/', $preview_video, $matches) ) {
		$src = $matches[1];
		$params = array(
                'playsinline' => 1,
				'controls'    => 0,
				'hd'  => 1,
				'autoplay' => 1,
				'background' => 1,
				'loop' => 1,
				'byline' => 0,
				'title' => 0,
                'muted' => 1
			);
			$new_src = add_query_arg($params, $src);
			$preview_video = str_replace($src, $new_src, $preview_video);
			$attributes = 'frameborder="0" muted loop playsinline webkit-playsinline';
			$preview_video = str_replace('></iframe>', ' ' . $attributes . '></iframe>', $preview_video);
		}
	echo $preview_video;
	?>
</div>
<div class="full-screen-video__popup">
	<div class="full-screen-video__popup__close">
		<?php echo get_img('close'); ?>
	</div>
	<div class="full-screen-video__popup__overlay"></div>
	<div class="full-screen-video__popup__inner">
	<?php if ( preg_match('/src="(.+?)"/', $full_video, $matches) ) {
		$src = $matches[1];
		$params = array(
                'playsinline' => 1,
				'controls'    => 0,
				'hd'  => 1,
				'autoplay' => 0,
				'background' => 1,
				'loop' => 1,
				'byline' => 0,
				'title' => 0,
                'muted' => 1
			);
			$new_src = add_query_arg($params, $src);
			$full_video = str_replace($src, $new_src, $full_video);
			$attributes = 'frameborder="0" muted loop playsinline webkit-playsinline';
			$full_video = str_replace('></iframe>', ' ' . $attributes . '></iframe>', $full_video);
		}
	echo $full_video;
	?>
	</div>
</div>
<?php endif; ?>
<?php
