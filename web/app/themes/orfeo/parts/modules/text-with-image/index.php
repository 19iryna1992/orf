<?php
$class = ! empty ( $args['class'] ) ? $args['class'] : '';

$subtitle       = get_sub_field( 'section_subtitle' );
$title          = get_sub_field( 'section_title' );
$description    = get_sub_field( 'section_description' );
$imageId        = get_sub_field( 'section_image' );
$image          = wp_get_attachment_image( $imageId, '' );
$list           = get_sub_field( 'list' );
$image_location = get_sub_field( 'image_location' );

echo load_inline_styles( __DIR__, $class );
?>
	<div class="container">
		<div class="text-with-image__row row text-with-image__row--<?php echo $image_location == 'left' ? 'left' : 'right'; ?>">
			<div class="text-with-image__col text-with-image__col--content col-12 col-lg-5">
				<?php if ( ! empty( $subtitle ) ) { ?>
					<h3 class="text-with-image__subtitle section-subtitle"><?php echo $subtitle; ?></h3>
				<?php } ?>
				<?php if ( ! empty( $title ) ) { ?>
					<h2 class="text-with-image__title h1"><?php echo $title; ?></h2>
				<?php } ?>
				<?php if ( ! empty( $description ) ) { ?>
					<div class="text-with-image__description"><?php echo $description; ?></div>
				<?php } ?>
			</div>
			<div class="text-with-image__col text-with-image__col--image col-12 col-lg-7">
				<?php if ( ! empty ( $image ) ) : echo $image; endif; ?>
			</div>
		</div>
		<?php if ( ! empty( $list ) ) { ?>
			<ul class="text-with-image__list">
			<?php foreach ( $list as $item ) :
				$text = $item['text'];
			?>
				<li class="text-with-image__list__item">
					<?php echo $text; ?>
				</li>
			<?php endforeach; ?>
			</ul>
		<?php } ?>
	</div>
<?php
