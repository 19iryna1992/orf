const removeActiveFromPopups = popups => {
	for (let index = 0; index < popups.length; index++) {
		const popup = popups[index];

		popup.classList.remove('active');
	}
};

const sliderInit = () => {
	const sections = document.querySelectorAll('.press');

	for (let index = 0; index < sections.length; index++) {
		const element = sections[index];
		const slider = element.querySelector('.press__slider');
		const more = element.querySelectorAll('.press__slide__button');
		const popups = element.querySelectorAll('.press__popup');

		const splide = new Splide(slider, {
			autoWidth: true,
			focus    : 0,
			omitEnd  : true,
			perMove: 1,
			gap: '62px',
			arrows: true,
			pagination: false,
			mediaQuery: 'max',
			breakpoints: {
				991: {
					gap: '32px',
				},
			},
		});
		splide.mount();

		for (let index = 0; index < more.length; index++) {
			const button = more[index];

			button.addEventListener('click', () => {
				const number = button.dataset.press;

				removeActiveFromPopups(popups);
				element.querySelector(`.press__popup[data-press="${number}"]`).classList.add('active');
				document.documentElement.style.overflow = 'hidden';
			});
		}

		for (let index = 0; index < popups.length; index++) {
			const popup = popups[index];
			const close = popup.querySelector('.press__popup__close');
			const overlay = popup.querySelector('.press__popup__overlay');

			close.addEventListener('click', () => {
				popup.classList.remove('active');
				document.documentElement.style.overflow = 'unset';
			});

			overlay.addEventListener('click', () => {
				popup.classList.remove('active');
				document.documentElement.style.overflow = 'unset';
			});
		}
	}
};

sliderInit();
