<?php
$class = ! empty ( $args['class'] ) ? $args['class'] : '';
$module_class = str_replace( '_', '-', $class );

load_blocks_script($module_class, 'orfeo/' . $module_class, '', 'parts/modules');
echo load_inline_styles( __DIR__, $class );

$main_text      = get_sub_field( 'main_text' );
$appearing_text = get_sub_field( 'appearing_text' );
$socials        = get_field( 'social_links', 'options' );
$logoId         = get_field( 'footer_logo', 'options' );
$logo           = wp_get_attachment_image( $logoId, 'hero-logo' );
?>
<?php if ( ! empty( $main_text ) ) :
$first_character = mb_substr($main_text, 0, 1);
?>
<div class="hero__title h1" data-letter="<?php echo $first_character; ?>" data-content="<?php echo $main_text; ?>">
	<span><?php echo $logo; ?></span>
	<span class="hero__title--after"><?php echo $logo; ?></span>
</div>
<?php if ( ! empty( $appearing_text ) ) : ?>
<h2 class="hero__description">
	<?php echo $appearing_text; ?>
</h2>
<?php endif; ?>
<?php if ( ! empty( $socials ) ) : ?>
<ul class="hero__socials">
	<?php foreach ( $socials as $item ) :
	$icon  = $item['icon'];
	$url   = $item['url'];
	$title = $item['title'];
	?>
	<?php if ( ! empty( $icon ) ) : ?>
	<li class="hero__social">
		<a href="<?php echo $url; ?>" target="_blank">
			<?php echo get_img( $icon ); ?>
			<span class="screen-reader-text"><?php echo $title; ?></span>
		</a>
	</li>
	<?php endif; ?>
	<?php endforeach; ?>
</ul>
<?php endif; ?>
<div class="hero__arrow">
	<?php echo get_img( 'arrow-down' ); ?>
</div>
<div class="hero__circles rounded-circles-animate">
	<div class="hero__circle hero__circle--smaller rounded-circle-animate rounded-circle-animate--smaller">
		<svg width="445" height="445" viewBox="0 0 445 445" fill="none" xmlns="http://www.w3.org/2000/svg">
			<circle cx="222.5" cy="222.5" r="222" stroke="white" stroke-dasharray="8 9"/>
		</svg>
	</div>
	<div class="hero__circle hero__circle--bigger rounded-circle-animate rounded-circle-animate--bigger">
		<svg width="650" height="650" viewBox="0 0 650 650" fill="none" xmlns="http://www.w3.org/2000/svg">
			<circle cx="325" cy="325" r="324.5" stroke="white" stroke-dasharray="8 9"/>
		</svg>
	</div>
</div>
<?php endif; ?>