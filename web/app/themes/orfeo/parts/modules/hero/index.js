const heroManageScrollClass = (heros, direction = 0) => {
	for (let i = 0; i < heros.length; i++) {
		const element = heros[i];

		if (direction === 1) {
			element.classList.add('scroll-down');
			element.classList.add('scroll-down-default');
		} else {
			element.classList.remove('scroll-down');
			setTimeout(() => {
				element.classList.remove('scroll-down-default');
			}, 0);
		}
	}
};

const heroInit = () => {
	const heros = document.querySelectorAll('.hero');

	for (let i = 0; i < heros.length; i++) {
		const element = heros[i];

		let duration = 4000;

		if (window.matchMedia('(max-width: 991px)').matches) {
			duration = 4000;
		}

		setTimeout(() => {
			element.classList.remove('loading');
			element.classList.add('active');
			document.querySelector('.main-header').classList.remove('loading');
		}, duration);
	}

	let lastScrollTop = 0;

	window.addEventListener('scroll', () => {
		const st = window.pageYOffset || document.documentElement.scrollTop;

		if (st > lastScrollTop) {
			heroManageScrollClass(heros, 1);
		} else if (st < lastScrollTop) {
			heroManageScrollClass(heros, 0);
		}

		lastScrollTop = st <= 0 ? 0 : st;
	}, false);
};

heroInit();
