<?php
$class = ! empty ( $args['class'] ) ? $args['class'] : '';
$module_class = str_replace( '_', '-', $class );

$subtitle        = get_sub_field( 'section_subtitle' );
$title           = get_sub_field( 'section_title' );
$description     = get_sub_field( 'section_description' );
$imageId         = get_sub_field( 'section_image' );
$image           = wp_get_attachment_image( $imageId, '' );
$video           = get_sub_field( 'video' );
$text_displaying = get_sub_field( 'text_displaying' );

load_inline_styles( __DIR__, $class );
echo load_blocks_script( $module_class, '' . $module_class, [], 'parts/modules' );
?>

<div class="container">
	<div class="realisations__row realisations__row--<?php echo $text_displaying; ?> realisations__row--<?php echo empty( $image ) ? ' realisations__row--no-image' : '' ; ?>">
		<?php if ( ! empty( $image ) ) : ?>
		<div class="realisations__image">
			<?php echo $image; ?>
		</div>
		<?php endif; ?>
		<?php if ( ! empty( $video ) ) : ?>
		<div class="realisations__video<?php echo $text_displaying != 'block' ? ' mobile' : ' realisations__video--block'; ?>">
			<video loop muted autoplay playsinline="true" preload="metadata">
				<source src="<?php echo $video['url']; ?>" type="<?php echo $video['mime_type']; ?>">
			</video>
		</div>
		<?php endif; ?>
		<div class="realisations__right realisations__right--<?php echo $text_displaying; ?>">
			<?php if ( ! empty( $subtitle ) ) : ?>
			<h3 class="realisations__subtitle section-subtitle"><?php echo $subtitle; ?></h3>
			<?php endif; ?>
			<?php if ( ! empty( $title ) ) : ?>
			<h2 class="realisations__title h2 text-revealup-animate"><?php echo $title; ?></h2>
			<?php endif; ?>
			<div class="realisations__bottom">
				<?php if ( ! empty( $video ) && $text_displaying != 'block' ) : ?>
				<div class="realisations__video desktop">
					<video loop muted autoplay playsinline="true" preload="metadata">
						<source src="<?php echo $video['url']; ?>" type="<?php echo $video['mime_type']; ?>">
					</video>
				</div>
				<?php endif; ?>
				<?php if ( ! empty( $description ) ) : ?>
				<div class="realisations__description"><?php echo $description; ?></div>
				<?php endif; ?>
			</div>
		</div>
	</div>
</div>

