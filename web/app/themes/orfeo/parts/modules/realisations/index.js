const scriptInit = () => {
	const sections = document.querySelectorAll('.realisations');

	for (let index = 0; index < sections.length; index++) {
		const section = sections[index];
		const next = section.nextElementSibling;
		let image = '';

		if (next.classList.contains('roadmap')) {
			image = next.nextElementSibling;
		}

		if (image !== '') {
			if (!image.classList.contains('full-screen-image')) {
				next.classList.add('roadmap--simple');
			}
		}

		const blocks = section.querySelectorAll('.realisations__video, .realisations__image');

		const observer = new IntersectionObserver(entries => {
			entries.forEach(entry => {
				if (entry.isIntersecting) {
					entry.target.classList.add('animate');
				}
			});
		}, { threshold: 0 });

		blocks.forEach(block => {
			observer.observe(block);
		});
	}
};

scriptInit();