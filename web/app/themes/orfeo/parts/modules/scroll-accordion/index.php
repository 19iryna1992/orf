<?php
$class = ! empty ( $args['class'] ) ? $args['class'] : '';
$module_class = str_replace( '_', '-', $class );

load_blocks_script($module_class, 'orfeo/' . $module_class, '', 'parts/modules');
echo load_inline_styles( __DIR__, $class );

$accordion_list = get_sub_field( 'accordion_list' );
?>
<?php if ( ! empty( $accordion_list ) ) : ?>
<div class="scroll-accordion__container container">
	<div class="scroll-accordion__content">
		<ul class="scroll-accordion__list">
			<li class="scroll-accordion__buttons">
			<?php $int = 1;
			foreach ( $accordion_list as $row ) :
			$title = isset( $row['title'] ) ? $row['title'] : '';
			if ( !empty($title) ) : ?>
				<div class="scroll-accordion__button<?php if ( $int === 1 ): ?> active dashed<?php endif; ?>" data-item="tab-<?php echo $int; ?>"><span><?php echo $title; ?></span></div>
			<?php
			$int++;
			endif;
			endforeach; ?>
			</li>
			<?php $int = 1;
			foreach ( $accordion_list as $row ) :
			$title             = isset( $row['title'] ) ? $row['title'] : '';
			$title_description = isset( $row['title_description'] ) ? $row['title_description'] : '';
			$description       = isset( $row['description'] ) ? $row['description'] : '';
			$link              = isset( $row['link'] ) ? $row['link'] : '';
			$imageId           = isset( $row['image'] ) ? $row['image'] : '';
			$image_caption     = isset( $row['image_caption'] ) ? $row['image_caption'] : '';

			if ( !empty($title) ) : ?>
			<li class="scroll-accordion__list-item<?php if ( $int === 1 ): ?> active<?php endif; ?>" data-item="tab-<?php echo $int; ?>">
				<div class="scroll-accordion__list-item__inner">
					<div class="scroll-accordion__list-left">
						<div class="scroll-accordion__list-left__inner">
							<h2 class="scroll-accordion__list-title h1 <?php if ( $int === 1 ): ?> active<?php endif; ?>"><?php echo $title; ?></h2>
							<?php if ( ! empty( $title_description ) ) : ?>
							<div class="scroll-accordion__list-title__description"><?php echo $title_description; ?></div>
							<?php endif; ?>
						</div>
						<?php if ( ! empty( $description ) ) : ?>
							<div class="scroll-accordion__list-description"><?php echo $description; ?></div>
						<?php endif; ?>
					</div>
					<?php if ( ! empty( $imageId ) ) :
					$image = wp_get_attachment_image( $imageId, '' );
					?>
					<div class="scroll-accordion__list-right">
						<div class="scroll-accordion__list-right__image">
							<?php echo $image; ?>
						</div>
						<?php if ( ! empty( $image_caption ) ) : ?>
						<div class="scroll-accordion__list-right__caption"><?php echo $image_caption; ?></div>
						<?php endif; ?>
					</div>
					<?php endif; ?>
				</div>
				<?php if ( ! empty( $link ) ) : ?>
					<div class="scroll-accordion__list-link">
						<?php echo get_button( $link , '', '', 'icon-dot' ); ?>
					</div>
				<?php endif; ?>
			</li>
			<?php
			$int++;
			endif;
			endforeach;
			?>
		</ul>
	</div>
	<?php if ( ! empty( $int ) ) : ?>
	<div class="scroll-accordion__container__helper__list">
		<?php for ($i=1; $i < $int; $i++) : ?>
		<div class="scroll-accordion__container__helper" data-item="tab-<?php echo esc_attr( $i ); ?>"></div>
		<?php endfor; ?>
	</div>
	<?php endif; ?>
</div>
<?php endif; ?>