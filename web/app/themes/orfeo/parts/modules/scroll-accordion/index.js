let isScrolled = false;

const addDashedToAllPrev = tabTitles => {
	let num = 0;
	for (let i = 0; i < tabTitles.length; i++) {
		const element = tabTitles[i];
		if (element.classList.contains('active')) {
			num = 1;
		}

		if (num === 0) {
			element.classList.add('dashed');
		}
	}
};

const clearTitleActiveStates = tabTitles => {
	for (let i = 0; i < tabTitles.length; i++) {
		const element = tabTitles[i];
		element.classList.remove('active');
		element.classList.remove('dashed');
	}
};

const clearContentActiveStates = tabContents => {
	for (let i = 0; i < tabContents.length; i++) {
		const element = tabContents[i];
		element.classList.remove('active');
	}
};

const updateHelperSize = function () {
	document.body.style.setProperty('--scroll-helper-height', parseInt(window.innerHeight * 0.9) + 'px');
};

const bindEvents = (tabsContainer, tabTitles, tabContents) => {
	updateHelperSize();

	for (let i = 0; i < tabTitles.length; i++) {
		const tabTitle = tabTitles[i];
		const tabId = tabTitle.getAttribute('data-item');

		tabTitle.querySelector('span').addEventListener('click', (event) => {
			if (tabTitle.classList.contains('active')) {
				return;
			}

			clearTitleActiveStates(tabTitles);
			clearContentActiveStates(tabContents);

			tabTitle.classList.add('active');
			addDashedToAllPrev(tabTitles);

			tabsContainer.querySelector(`.scroll-accordion__list-item[data-item="${tabId}"]`).classList.add('active');

			if (event.isTrusted) {
				const currentHelper = tabsContainer.querySelector(`.scroll-accordion__container__helper[data-item="${tabId}"]`);

				if (currentHelper !== null) {
					isScrolled = true;

					setTimeout(() => {
						isScrolled = false;
					}, 500);

					currentHelper.scrollIntoView({ block: 'center', behavior: 'instant' });
				}
			}
		});
	}

	window.addEventListener('resize', () => {
		updateHelperSize();
	});
};

const scrollTabsObserverEvents = function (tabsContainer) {
	const helperList = tabsContainer.querySelectorAll('.scroll-accordion__container__helper');

	const inViewObserver = new IntersectionObserver(nodes => {
		nodes.forEach(node => {
			if (!isScrolled && node.isIntersecting) {
				const tabId      = node.target.dataset.item;
				const currentTab = tabsContainer.querySelector(`.scroll-accordion__button[data-item="${tabId}"]`);

				if (currentTab !== null && ! currentTab.classList.contains('active')) {
					currentTab.querySelector('span').click();
				}
			}
		});
	}, { threshold: 1 });

	[].forEach.call(helperList, function (helperElem) {
		inViewObserver.observe(helperElem);
	});
};

const updateLargeScreenPosition = tabsList => {
	const diff = (window.innerHeight - tabsList.offsetHeight) / 2;
	let lPos = `${170}px`;

	if (diff > 170) {
		lPos = `${diff}px`;
	}

	tabsList.style.setProperty('--l-s-top-pos', lPos);
};

const scrollTabsInit = () => {
	const scrollTabs = document.querySelectorAll('.scroll-accordion');

	for (let i = 0; i < scrollTabs.length; i++) {
		const element = scrollTabs[i];

		const tabTitles = element.querySelectorAll('.scroll-accordion__button');
		const tabContents = element.querySelectorAll('.scroll-accordion__list-item');
		const tabsContainer = element.querySelector('.scroll-accordion__container');
		const tabsListEl = element.querySelector('.scroll-accordion__list');

		bindEvents(tabsContainer, tabTitles, tabContents);

		if (tabsContainer) {
			scrollTabsObserverEvents(tabsContainer, tabContents);
		}

		window.addEventListener('resize', () => {
			updateLargeScreenPosition(tabsListEl);
		});

		document.addEventListener('DOMContentLoaded', () => {
			updateLargeScreenPosition(tabsListEl);
		});

		// window.addEventListener('ajaxLoaded', () => {
		// 	ajaxTimeout = setTimeout(() => {
		// 		updateLargeScreenPosition(tabsListEl);
		// 	}, 300);
		// }, {once : true});
	}
};

scrollTabsInit();