const formInit = () => {
	const sections = document.querySelectorAll('.contact-form');

	for (let index = 0; index < sections.length; index++) {
		const element = sections[index];
		// const cf7Form = element.querySelector('.wpcf7');
		// const formInner = element.querySelector('.contact-form__form');

		// should be used once forms are configured
		// cf7Form.addEventListener('wpcf7mailsent', () => {
		// });

		// Used only for testing purposes
		// cf7Form.addEventListener('wpcf7submit', () => {
		// 	location.href = link;
		// });

		const queryString = window.location.search;
		const urlParams = new URLSearchParams(queryString);
		const select = element.querySelector('.cf7-input-row--select .wpcf7-select');
		const id = element.querySelector('#contact-form');

		if (window.location.hash !== '') {
			setTimeout(() => {
				window.scrollTo({
					top: id.getBoundingClientRect().top,
					behavior: 'smooth',
				});
			}, 300);

			const selectParam = urlParams.get('select');
			select.value = selectParam;
		}
	}
};

formInit();
