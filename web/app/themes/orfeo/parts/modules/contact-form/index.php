<?php
$class = ! empty ( $args['class'] ) ? $args['class'] : '';
$module_class = str_replace( '_', '-', $class );

$subtitle          = get_sub_field( 'section_subtitle' );
$title             = get_sub_field( 'section_title' );
$description       = get_sub_field( 'section_description' );
$form              = get_sub_field( 'form_code' );

load_inline_styles( __DIR__, $class );
load_inline_dependencies('/parts/gutenberg/core-button/', 'core-button');

echo load_blocks_script( $module_class, '' . $module_class, [], 'parts/modules' ); ?>
<div class="contact-form__container container">
	<div class="contact-form__row" id="contact-form">
		<div class="contact-form__content">
			<?php if ( !empty( $subtitle ) ) : ?>
			<h2 class="contact-form__subtitle section-subtitle"><?php echo $subtitle; ?></h2>
			<?php endif; ?>
			<?php if ( !empty( $title ) ) : ?>
			<h2 class="contact-form__title h1"><?php echo $title; ?></h2>
			<?php endif; ?>
			<?php if ( ! empty( $description ) ) : ?>
			<div class="contact-form__description"><?php echo $description; ?></div>
			<?php endif; ?>
		</div>
		<?php if ( !empty( $form ) ) : ?>
		<div class="contact-form__form">
			<?php echo do_shortcode( $form ); ?>
		</div>
		<?php endif; ?>
	</div>
</div>
