<?php
$class = ! empty ( $args['class'] ) ? $args['class'] : '';

$title       = get_sub_field( 'section_title' );
$description = get_sub_field( 'section_description' );

if ( ! empty( $title ) ) {
	echo load_inline_styles( __DIR__, $class );
	?>
	<div class="container container--narrow">
		<h2 class="intro__title h1 text-revealup-animate"><?php echo $title; ?></h2>
		<?php if ( ! empty( $description ) ) { ?>
			<div class="intro__description text--large"><?php echo $description; ?></div>
		<?php }; ?>
	</div>
	<?php
}
