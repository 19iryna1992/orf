<?php
$class = ! empty ( $args['class'] ) ? $args['class'] : '';

echo load_inline_styles( __DIR__, $class );

$hero_title  = get_sub_field( 'hero_title' );
$title       = get_sub_field( 'section_title' );
$description = get_sub_field( 'section_description' );

?>
<?php if ( ! empty( $hero_title ) ) : ?>
<h2 class="text__hero h1">
	<span><?php echo $hero_title; ?></span>
</h2>
<?php endif; ?>
<div class="text__container container">
	<?php if ( ! empty( $title ) ) : ?>
	<h3 class="text__title h1"><?php echo $title; ?></h3>
	<?php endif; ?>
	<?php if ( ! empty( $description ) ) : ?>
	<div class="text__description"><?php echo $description; ?></div>
	<?php endif; ?>
</div>