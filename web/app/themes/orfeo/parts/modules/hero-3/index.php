<?php
$class = ! empty ( $args['class'] ) ? $args['class'] : '';
$module_class = str_replace( '_', '-', $class );

echo load_inline_styles( __DIR__, $class );

$section_title = get_sub_field( 'section_title' );
$section_description   = get_sub_field( 'section_description' );
?>
<?php if ( ! empty( $section_title ) || ! empty( $section_description ) ) : ?>
<button class="hero-3__back button-back">
	<?php echo get_img( 'arrow-left', '' ); ?>
	<?php echo _e('Retour', 'orfeo'); ?>
</button>
<div class="container">
	<div class="hero-3__text">
		<?php if ( ! empty( $section_title ) ) : ?>
		<h2 class="hero-3__title h1">
			<?php echo $section_title; ?>
		</h2>
		<?php endif; ?>
		<?php if ( ! empty( $section_description ) ) : ?>
		<div class="hero-3__description">
			<?php echo $section_description; ?>
		</div>
		<?php endif; ?>
	</div>
</div>
<div class="hero-3__circles rounded-circles-animate rounded-circles-animate-scroll">
	<div class="hero-3__circle hero-3__circle--smaller rounded-circle-animate rounded-circle-animate--smaller">
		<svg width="890" height="890" viewBox="0 0 890 890" fill="none" xmlns="http://www.w3.org/2000/svg">
			<circle cx="445" cy="445" r="444" stroke="white" stroke-opacity="0.3" stroke-width="2" stroke-dasharray="17 17"/>
		</svg>
	</div>
	<div class="hero-3__circle hero-3__circle--bigger rounded-circle-animate rounded-circle-animate--bigger">
		<svg width="1300" height="1300" viewBox="0 0 1300 1300" fill="none" xmlns="http://www.w3.org/2000/svg">
			<circle cx="650" cy="650" r="649" stroke="white" stroke-opacity="0.25" stroke-width="2" stroke-dasharray="18 18"/>
		</svg>
	</div>
</div>
<?php endif; ?>