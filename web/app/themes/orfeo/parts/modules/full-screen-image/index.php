<?php
$class = ! empty ( $args['class'] ) ? $args['class'] : '';

$imageId = get_sub_field( 'image' );
$image   = wp_get_attachment_image( $imageId, 'full-screen-image' );

echo load_inline_styles( __DIR__, $class );
?>

<?php if ( ! empty( $image ) ) : ?>
<div class="full-screen-image__image">
	<?php echo $image; ?>
</div>
<?php endif; ?>
