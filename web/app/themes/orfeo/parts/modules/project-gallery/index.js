const sliderInit = () => {
	const sections = document.querySelectorAll('.project-gallery');

	for (let index = 0; index < sections.length; index++) {
		const element = sections[index];
		const slider = element.querySelector('.project-gallery__slider');

		const splide = new Splide(slider, {
			autoWidth: true,
			focus    : 0,
			omitEnd  : true,
			perMove: 1,
			gap: '20px',
			arrows: true,
			pagination: false,
		});
		splide.mount();
	}
};

sliderInit();
