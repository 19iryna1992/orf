<?php
$class = ! empty ( $args['class'] ) ? $args['class'] : '';
$module_class = str_replace( '_', '-', $class );

$subtitle         = get_sub_field( 'section_subtitle' );
$title            = get_sub_field( 'section_title' );
$description      = get_sub_field( 'section_description' );
$imageId          = get_sub_field( 'section_image' );
$image            = wp_get_attachment_image( $imageId, 'news' );
$socials_title    = get_sub_field( 'socials_title' );
$socials_list     = get_sub_field( 'socials_list' );
$button_more_text = get_sub_field( 'button_more_text' );

load_inline_styles( __DIR__, $class );
?>

<div class="container news__container">
<?php if ( ! empty( $socials_list ) ) : ?>
	<?php if ( !empty( $subtitle ) ) : ?>
	<h2 class="news__subtitle section-subtitle"><?php echo $subtitle; ?></h2>
	<?php endif; ?>
	<?php if ( !empty( $title ) ) : ?>
	<h2 class="news__title h1"><?php echo $title; ?></h2>
	<?php endif; ?>
	<?php if ( !empty( $description ) ) : ?>
	<div class="news__description"><?php echo $description; ?></div>
	<?php endif; ?>
	<div class="news__row">
		<div class="news__socials">
			<?php if ( !empty( $socials_title ) ) : ?>
			<h3 class="news__title--list h3"><?php echo $socials_title; ?></h3>
			<?php endif; ?>
			<ul class="news__list">
			<?php foreach( $socials_list as $socials_item ):
				$iconId  = $socials_item['icon'];
				$icon = wp_get_attachment_image( $iconId, 'socials' );
				$link  = $socials_item['link'];
			?>
				<li class="news__item">
					<?php if ( !empty( $icon ) && !empty( $link ) ) : ?>
					<a class="news__item__link" href="<?php echo $link['url']; ?>">
						<?php if ( !empty( $icon ) ) : ?>
							<div class="news__item__icon">
								<?php echo $icon; ?>
							</div>
						<?php endif; ?>
						<?php if ( !empty( $link['title'] ) ) : ?>
							<div class="news__item__name">
								<?php echo $link['title']; ?>
							</div>
						<?php endif; ?>
					</a>
					<?php endif; ?>
				</li>
			<?php endforeach; ?>
			</ul>
		</div>
		<?php if ( !empty( $image ) ) : ?>
		<div class="news__image"><?php echo $image; ?></div>
		<?php endif; ?>
	</div>
<?php endif; ?>
</div>

