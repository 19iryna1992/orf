<?php
$class = ! empty ( $args['class'] ) ? $args['class'] : '';
$module_class = str_replace( '_', '-', $class );

$subtitle = get_sub_field( 'section_subtitle' );
$title    = get_sub_field( 'section_title' );
$address  = get_sub_field( 'address' );
$phone    = get_sub_field( 'phone' );
$email    = get_sub_field( 'email' );
$map      = get_sub_field( 'map_url' );

load_inline_styles( __DIR__, $class );

// echo load_blocks_script( $module_class, 'splide/' . $module_class, ['splide'], 'parts/modules' ); ?>
<div class="contact-map__container container">
	<div class="contact-map__row">
		<div class="contact-map__content">
			<?php if ( !empty( $subtitle ) ) : ?>
			<h2 class="contact-map__subtitle section-subtitle"><?php echo $subtitle; ?></h2>
			<?php endif; ?>
			<?php if ( !empty( $title ) ) : ?>
			<h2 class="contact-map__title h1"><?php echo $title; ?></h2>
			<?php endif; ?>
			<div class="contact-map__info">
				<?php if ( !empty( $address ) ) : ?>
				<div class="contact-map__info__inner contact-map__address">
					<?php if ( !empty( $address['icon'] ) ) : ?>
					<div class="contact-map__icon">
						<?php echo get_img( $address['icon'] ); ?>
					</div>
					<?php endif; ?>
					<?php if ( !empty( $address['text'] ) ) : ?>
					<div class="contact-map__text">
						<?php echo $address['text']; ?>
					</div>
					<?php endif; ?>
				</div>
				<?php endif; ?>
				<?php if ( !empty( $phone ) ) : ?>
				<div class="contact-map__info__inner contact-map__phone">
					<?php if ( !empty( $phone['icon'] ) ) : ?>
					<div class="contact-map__icon">
						<?php echo get_img( $phone['icon'] ); ?>
					</div>
					<?php endif; ?>
					<?php if ( !empty( $phone['text'] ) ) :
					$phoneNumber = str_replace(' ', '', $phone['text']);
					?>
					<a class="contact-map__text" href="tel:<?php echo $phoneNumber; ?>">
						<?php echo $phone['text']; ?>
					</a>
					<?php endif; ?>
				</div>
				<?php endif; ?>
				<?php if ( !empty( $email ) ) : ?>
				<div class="contact-map__info__inner contact-map__email">
					<?php if ( !empty( $email['icon'] ) ) : ?>
					<div class="contact-map__icon">
						<?php echo get_img( $email['icon'] ); ?>
					</div>
					<?php endif; ?>
					<?php if ( !empty( $email['text'] ) ) : ?>
					<a class="contact-map__text" href="mailto:<?php echo $email['text']; ?>">
						<?php echo $email['text']; ?>
					</a>
					<?php endif; ?>
				</div>
				<?php endif; ?>
			</div>
		</div>
		<?php if ( !empty( $map ) ) : ?>
		<div class="contact-map__map">
			<iframe src="<?php echo $map; ?>" width="758" height="450" style="border:0;" allowfullscreen="" loading="lazy" referrerpolicy="no-referrer-when-downgrade"></iframe>
		</div>
		<?php endif; ?>
	</div>
</div>
