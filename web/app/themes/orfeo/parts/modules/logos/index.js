const sliderInit = () => {
	const sections = document.querySelectorAll('.logos');

	for (let index = 0; index < sections.length; index++) {
		const element = sections[index];
		const slider = element.querySelector('.logos__slider');

		const splide = new Splide(slider, {
			perPage: 1,
			perMove: 1,
			updateOnMove: true,
			grid: {
				dimensions: [[2, 6]],
				gap: {
					row: 60,
				},
			},
			arrows: true,
			pagination: false,
			speed: 1200,
			mediaQuery: 'max',
			breakpoints: {
				991: {
					destroy: true,
				},
			},
		}).mount(window.splide.Extensions);


		const blocks = element.querySelectorAll('.splide__slide:not(.logos__slide) .splide__slide__row');

		const observer = new IntersectionObserver(entries => {
			entries.forEach(entry => {
				if (entry.isIntersecting) {
					entry.target.classList.add('animate');
				}
			});
		}, { threshold: 0 });

		blocks.forEach(block => {
			observer.observe(block);
		});

		window.addEventListener('resize', () => {
			const blocks = element.querySelectorAll('.splide__slide:not(.logos__slide) .splide__slide__row');

			blocks.forEach(block => {
				block.classList.add('animate');
			});
		});
	}
};

sliderInit();
