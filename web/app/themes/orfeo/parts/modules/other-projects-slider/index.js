const sliderInit = () => {
	const sections = document.querySelectorAll('.other-projects-slider');

	for (let index = 0; index < sections.length; index++) {
		const element = sections[index];
		const slider = element.querySelector('.other-projects-slider__slider');

		const splide = new Splide(slider, {
			perPage: 1,
			perMove: 1,
			gap: '40px',
			arrows: true,
			pagination: false,
			focus: 0,
			destroy: false,
			mediaQuery: 'max',
			breakpoints: {
				992: {
					destroy: true,
				},
			},
		});
		splide.mount();
	}
};

sliderInit();
