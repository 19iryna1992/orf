const isOdd = number => {
	return number % 2 !== 0;
};

const sliderInit = () => {
	const sections = document.querySelectorAll('.notre-adn');

	for (let index = 0; index < sections.length; index++) {
		const element = sections[index];
		const sliders = element.querySelectorAll('.notre-adn__slider');

		for (let index = 0; index < sliders.length; index++) {
			const slider = sliders[index];

			let speed = 1;

			if (isOdd(index)) {
				speed = -1;
			}

			const splide = new Splide(slider, {
				type   : 'loop',
				autoWidth: true,
				drag   : 'false',
				arrows: false,
				pagination: false,
				rewind: false,
				focus  : 'center',
				gap: '236px',
				autoScroll: {
					speed: speed,
				},
				breakpoints: {
					991: { gap: '40px' },
				},
			});
			splide.mount(window.splide.Extensions);

			const lines = element.querySelectorAll('.notre-adn__line');

			for (let index = 0; index < lines.length; index++) {
				const line = lines[index];

				line.addEventListener('mousemove', e => {
					// const rect = e.target.getBoundingClientRect();
					const left = e.clientX;
					// const top = e.layerY;
					const file = line.querySelector('.notre-adn__line__file');
					const width = file.offsetWidth;
					// const height = file.offsetHeight;

					file.style.left = (left - (width / 2)) + 'px';
					// file.style.top = (top - (height / 2)) + 'px';
				});
			}
		}
	}
};

sliderInit();
