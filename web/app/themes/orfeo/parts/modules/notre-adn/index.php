<?php
$class = ! empty ( $args['class'] ) ? $args['class'] : '';
$module_class = str_replace( '_', '-', $class );

echo load_inline_styles( __DIR__, $class );
load_inline_styles_plugin( 'splide.min' );
load_inline_styles_shared( 'sliders' );
load_blocks_script( $module_class, 'splide/' . $module_class, ['splide-auto-scroll', 'splide'], 'parts/modules' );

$subtitle    = get_sub_field( 'section_subtitle' );
$title       = get_sub_field( 'section_title' );
$description = get_sub_field( 'section_description' );
$lines       = get_sub_field( 'lines' );
$link        = get_sub_field( 'link' );
?>

<div class="notre-adn__row">
	<div class="container">
	<?php if ( ! empty( $subtitle ) ) : ?>
		<h3 class="notre-adn__subtitle section-subtitle"><?php echo $subtitle; ?></h3>
	<?php endif; ?>
	<?php if ( ! empty( $title ) ) : ?>
		<div class="notre-adn__title h1"><?php echo $title; ?></div>
	<?php endif; ?>
	<?php if ( ! empty( $description ) ) : ?>
		<div class="notre-adn__description"><?php echo $description; ?></div>
	<?php endif; ?>
	</div>
	<?php if ( ! empty( $lines ) ) : ?>
	<ul class="notre-adn__lines">
	<?php foreach( $lines as $key => $line ): ?>
		<?php if ( ! empty( $line['line_text'] ) && ! empty( $line['line_file'] ) ) : ?>
		<li class="notre-adn__line">
			<div class="notre-adn__line__text notre-adn__slider splide">
				<div class="splide__track">
					<ul class="splide__list">
						<li class="splide__slide">
							<span data-text="<?php echo $line['line_text']; ?>"><?php echo $line['line_text']; ?></span>
						</li>
						<li class="splide__slide">
							<span data-text="<?php echo $line['line_text']; ?>"><?php echo $line['line_text']; ?></span>
						</li>
						<li class="splide__slide">
							<span data-text="<?php echo $line['line_text']; ?>"><?php echo $line['line_text']; ?></span>
						</li>
						<li class="splide__slide">
							<span data-text="<?php echo $line['line_text']; ?>"><?php echo $line['line_text']; ?></span>
						</li>
						<li class="splide__slide">
							<span data-text="<?php echo $line['line_text']; ?>"><?php echo $line['line_text']; ?></span>
						</li>
					</ul>
				</div>
			</div>
			<div class="notre-adn__line__file">
				<?php if ( $line['line_file']['mime_type'] == 'image/png' || $line['line_file']['mime_type'] == 'image/gif' || $line['line_file']['mime_type'] == 'image/jpeg' ) : ?>
					<img src="<?php echo $line['line_file']['url']; ?>" alt="<?php echo $line['line_file']['caption']; ?>">
				<?php endif; ?>
				<?php if ( $line['line_file']['mime_type'] == 'video/mp4' ) : ?>
					<video loop muted autoplay playsinline="true" preload="metadata">
						<source src="<?php echo $line['line_file']['url']; ?>" type="<?php echo $line['line_file']['mime_type']; ?>">
					</video>
				<?php endif; ?>
			</div>
		</li>
		<?php endif; ?>
	<?php endforeach; ?>
	</ul>
	<?php endif; ?>
	<?php if ( !empty( $link ) ) : ?>
		<div class="container">
			<div class="notre-adn__link"><?php echo get_button( $link , '', '', 'icon-dot' ); ?>
		</div>
	<?php endif; ?>
</div>
