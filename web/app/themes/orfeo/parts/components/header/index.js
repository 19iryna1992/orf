const toggleLinks = document.querySelectorAll('.btn-menu');

Array.from(toggleLinks).forEach(link => {
	link.addEventListener('click', function () {
		const expanded = this.getAttribute('aria-expanded') === 'true' || false;

		if (!expanded) {
			setTimeout(() => {
				document.body.classList.add('menu-open');
				document.body.classList.add('no-scroll');
			}, 300);
		} else {
			document.body.classList.remove('menu-open');
			document.body.classList.remove('no-scroll');
		}

		this.setAttribute('aria-expanded', !expanded);
		const menu = this.nextElementSibling;
		menu.classList.toggle('open');
		menu.hidden = !menu.hidden;

		const header = link.closest('.main-header');
		header.classList.toggle('open');
	});
});

const hoverSubmenu = document.querySelectorAll('.main-header__desktop-nav .menu-item-has-children');
Array.from(hoverSubmenu).forEach(link => {
	link.addEventListener('mouseover', function () {
		const dropdown = this.querySelector('.menu-item__dropdown');
		if (dropdown.getAttribute('aria-expanded') !== 'true') {
			const expanded = dropdown.getAttribute('aria-expanded') === 'true' || false;
			dropdown.setAttribute('aria-expanded', !expanded);
			const menu = this.querySelector('.menu-item__submenu');
			menu.hidden = false;
		}
	});
	link.addEventListener('mouseout', function () {
		const dropdown = this.querySelector('.menu-item__dropdown');
		const expanded = dropdown.getAttribute('aria-expanded') === 'true' || false;
		dropdown.setAttribute('aria-expanded', !expanded);
		const menu = this.querySelector('.menu-item__submenu');
		menu.hidden = true;
	});
});

