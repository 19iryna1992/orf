<?php
$main_logo = get_field( 'header_logo', 'options');
$logoId         = get_field( 'footer_logo', 'options' );
$logo           = wp_get_attachment_image( $logoId, 'mobile-menu-logo' );
$social_links = get_field( 'social_links', 'options');

load_blocks_script('header', 'orfeo/header');
?>
<header class="main-header<?php echo is_front_page() ? ' loading' : ''; ?>">
	<div class="container">
		<?php load_inline_styles(__DIR__, 'header') ; ?>
		<div class="main-header__logo">
			<a href="<?php echo get_bloginfo( 'url' ); ?>" class="main-logo"><span class="screen-reader-text"><?php _e( 'Main Logo', 'orfeo' ); ?></span><?php echo get_img( $main_logo, 'full' ) ?></a>
		</div>
	<?php if ( has_nav_menu( 'navigation' ) ) : ?>
		<div class="main-header__nav">
			<nav class="main-header__desktop-nav" aria-labelledby="nav-heading">
				<span id="nav-heading" class="screen-reader-text"><?php _e( 'Main Navigation', 'orfeo' ); ?></span>
				<?php wp_nav_menu( array( 'theme_location' => 'navigation', 'container' => false, 'walker' => new Main_Nav_Menu_Walker() ) ); ?>
			</nav>

			<?php $current_lang = apply_filters( 'wpml_current_language', NULL );
			$languages = apply_filters( 'wpml_active_languages', NULL );
			?>
			<?php if ( ! empty( $languages ) ) : ?>
			<div class="main-header__languages">
				<a href="<?php echo $languages['fr']['url']; ?>" class="<?php echo $current_lang == 'fr' ? 'active' : ''; ?>">FR</a>
				/
				<a href="<?php echo $languages['en']['url']; ?>" class="<?php echo $current_lang == 'en' ? 'active' : ''; ?>">EN</a>
			</div>
			<?php endif;?>
		</div>
	<?php endif; ?>
		<button class="btn-menu" aria-haspopup="true" aria-expanded="false">
			<span></span>
			<span></span>
			<span></span>
			<span class="screen-reader-text"><?php _e('Menu', 'orfeo'); ?></span>
		</button>
		<div class="main-header__mobile" hidden>
			<div class="main-header__mobile__inner">
				<?php if ( ! empty( $logo ) ) : ?>
				<div class="main-header__mobile__logo">
					<a href="<?php echo get_bloginfo( 'url' ); ?>" class="main-logo"><span class="screen-reader-text"><?php _e( 'Main Logo', 'orfeo' ); ?></span><?php echo $logo; ?></a>
				</div>
				<?php endif;?>
				<nav class="main-header__mobile__nav" aria-labelledby="mobile-nav-heading">
					<span id="mobile-nav-heading" class="screen-reader-text"><?php _e( 'Mobile Navigation', 'orfeo' ); ?></span>
					<ul id="menu-main-navigation-0" class="menu">
						<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-98">
							<a href="/">Homepage</a>
						</li>
					</ul>
					<?php wp_nav_menu( array( 'theme_location' => 'navigation', 'container' => false, 'walker' => new Main_Nav_Menu_Walker() ) ); ?>
				</nav>
				<?php if ( ! empty( $languages ) ) : ?>
				<div class="main-header__mobile__languages">
				<a href="<?php echo $languages['fr']['url']; ?>" class="<?php echo $current_lang == 'fr' ? 'active' : ''; ?>">FR</a><a href="<?php echo $languages['en']['url']; ?>" class="<?php echo $current_lang == 'en' ? 'active' : ''; ?>">EN</a>
				</div>
				<?php endif;?>
			</div>
			<?php if ( ! empty( $social_links ) ) : ?>
			<ul class="main-header__mobile__socials">
				<?php foreach( $social_links as $social ):
					$icon = $social['icon'];
				?>
				<?php if ( ! empty( $icon ) ) : ?>
					<li class="main-header__mobile__social">
						<a href="<?php echo $social['url']; ?>">
							<?php echo get_img( $icon, 'full' ) ?>
						</a>
					</li>
				<?php endif;?>
				<?php endforeach; ?>
			</ul>
			<?php endif;?>
		</div>
	</div>
</header>
