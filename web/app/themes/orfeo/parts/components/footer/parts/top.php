<?php
$footer_logo          = get_field( 'footer_logo', 'options' );
$footer_logo_title    = get_field( 'footer_logo_title', 'options' );
$footer_address       = get_field( 'footer_address', 'options' );
$footer_number        = get_field( 'footer_number', 'options' );
$footer_first_column  = get_field( 'footer_first_column', 'options' );
$footer_second_column = get_field( 'footer_second_column', 'options' );
$footer_third_column  = get_field( 'footer_third_column', 'options' );
?>

<span class="main-footer__top-line dashed-line"></span>
<div class="main-footer__top">
	<div class="container">
		<div class="row">
			<?php
				if ( ! empty( $footer_logo ) ) {
					?>
					<div class="col-12 main-footer__logo main-footer__col">
						<a href="<?php echo get_bloginfo( 'url' ); ?>" class="footer-logo">
							<span class="screen-reader-text">
								<?php echo $footer_logo_title ? $footer_logo_title : _e( 'Footer Logo', 'orfeo' ) ; ?>
							</span>
							<?php echo get_img( $footer_logo, 'full' ) ?>
						</a>
					</div>
					<?php
				}
			?>
			<div class="col-12 main-footer__info">
				<?php
					if ( ! empty( $footer_address ) ) {
						?>
						<div class="main-footer__info-address"><?php echo $footer_address; ?></div>
						<?php
					}
				?>
				<?php
					if ( ! empty( $footer_number ) ) {
						?>
						<p class="main-footer__info-text"><?php echo esc_html( $footer_number ); ?></p>
						<?php
					}
				?>
			</div>
			<?php
				if ( ! empty( $footer_first_column ) ) {
					?>
					<div class="col-12 main-footer__menu main-footer__menu--col-first">
					<?php
						while ( have_rows ( 'footer_first_column', 'options' ) ) : the_row();
							$menu_item   = get_sub_field( 'menu_item' );
							$link_url    = $menu_item['url'];
							$link_title  = $menu_item['title'];
							$link_target = $menu_item['target'] ? $menu_item['target'] : '_self';
							?>
								<a class="main-footer__menu-link" href="<?php echo esc_url( $link_url ); ?>" target="<?php echo esc_attr( $link_target ); ?>"><?php echo $link_title; ?></a>
							<?php
						endwhile;
						?>
					</div>
					<?php
				}
			?>
			<?php
				if ( ! empty( $footer_second_column ) ) {
					?>
					<div class="col-12 main-footer__menu main-footer__menu--col-second">
					<?php
						while ( have_rows ( 'footer_second_column', 'options' ) ) : the_row();
							$menu_item   = get_sub_field( 'menu_item' );
							$link_url    = $menu_item['url'];
							$link_title  = $menu_item['title'];
							$link_target = $menu_item['target'] ? $menu_item['target'] : '_self';
							?>
								<a class="main-footer__menu-link" href="<?php echo esc_url( $link_url ); ?>" target="<?php echo esc_attr( $link_target ); ?>"><?php echo $link_title; ?></a>
							<?php
						endwhile;
						?>
					</div>
					<?php
				}
			?>
			<?php
				if ( ! empty( $footer_third_column ) ) {
					?>
					<div class="col-12 main-footer__menu main-footer__menu--col-third">
					<?php
						while ( have_rows ( 'footer_third_column', 'options' ) ) : the_row();
							$menu_item   = get_sub_field( 'menu_item' );
							$link_url    = $menu_item['url'];
							$link_title  = $menu_item['title'];
							$link_target = $menu_item['target'] ? $menu_item['target'] : '_self';
							?>
								<a class="main-footer__menu-link" href="<?php echo esc_url( $link_url ); ?>" target="<?php echo esc_attr( $link_target ); ?>"><?php echo $link_title; ?></a>
							<?php
						endwhile;
						?>
					</div>
					<?php
				}
			?>
			<div class="col-12 main-footer__social">
				<?php if ( have_rows( 'social_links', 'options' ) ) {
					?>
					<ul class="main-footer__social-list">
						<?php
							while ( have_rows ( 'social_links', 'options' ) ) : the_row();
								$icon  = get_sub_field( 'icon' );
								$url   = get_sub_field( 'url' );
								$title = get_sub_field( 'title' );

								if ( $icon && $url ) {
									?>
									<li class="main-footer__social-item"><a href="<?php echo esc_url( $url ); ?>" class="main-footer__social-item-link" target="_blank"><?php echo get_img( $icon ); ?><span class="screen-reader-text"><?php echo $title; ?></span></a></li>
									<?php
								}
							endwhile;
						?>
					</ul>
					<?php
				}
				?>
			</div>
		</div>
	</div>
</div>
