<?php
$copyright            = get_field( 'copyright', 'options' );
$copyright_additional = get_field( 'copyright_additional', 'options' );
?>

<div class="main-footer__bottom">
	<div class="container">
		<div class="copyright">
			<span class="copyright__text"><?php echo $copyright ?></span>
			<?php
				if ( ! empty( $copyright_additional ) ) {
					?>
					<div class="copyright__additional"><?php echo $copyright_additional; ?></div>
					<?php
				}
			?>
		</div>
	</div>
</div>