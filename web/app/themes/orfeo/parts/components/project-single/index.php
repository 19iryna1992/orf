<?php
$id = get_the_ID();

// for hero-3 section

$title = get_field('title');
$precise_location = get_field('precise_location');

$category = get_the_terms( $id, 'project-category' );
$type = get_the_terms( $id, 'project-type' );

$category_terms = '';
$type_terms = '';

if ( $category && ! is_wp_error( $category ) ) {
	foreach ( $category as $key => $term ) {
		if ($key !== array_key_first($category)) {
			$category_terms .= ' / ';
		}
		$category_terms .= $term->name;
	}
}

if ( $type && ! is_wp_error( $type ) ) {
	foreach ( $type as $key => $term ) {
		if ($key !== array_key_first($type)) {
			$type_terms .= ' / ';
		}
		$type_terms .= $term->name;
	}
}

// for title-with-text-image section
$subtitle         = get_field('subtitle');
$description      = get_field('description');
$featured_imageId = get_field('featured_image');
$featured_image   = wp_get_attachment_image( $featured_imageId, '' );

// for title-with-text-boxes section
$client        = get_field('client');
$delivery_date = get_field('delivery_date');
$surface_area  = get_field('surface_area');
$architects    = get_field('architects');
$taker         = get_field('taker');

$certification = get_the_terms( $id, 'project-certification' );

$certification_terms = '';

if ( $certification && ! is_wp_error( $certification ) ) {
    $certification_terms = apply_filters( 'taxonomy-images-list-the-terms', '', array(
        'before'       => '',
        'after'        => '',
        'before_image' => '<li class="title-with-text-boxes__logo">',
        'after_image'  => '</li>',
        'image_size'   => '',
        'post_id'      => $id,
        'taxonomy'     => 'project-certification',
    ));
}

// for realisations section
$title_timeline       = get_field('title_timeline');
$description_timeline = get_field('description_timeline');
$imageId_timeline     = get_field( 'image_timeline' );
$image_timeline        = wp_get_attachment_image( $imageId_timeline, '' );
$image2Id_timeline     = get_field( 'image_2_timeline' );
$image2_timeline       = wp_get_attachment_image( $image2Id_timeline, '' );


// for roadmap section
$step_1 = get_field('step_1');
$step_2 = get_field('step_2');
$step_3 = get_field('step_3');
$step_4 = get_field('step_4');

// for full-screen-image section
$main_imageId = get_field('main_image');
$main_image   = wp_get_attachment_image( $main_imageId, '' );

// for project-single-slider section
$description_slider  = get_field('description_slider');
$slider_project_view = get_field('slider_project_view');
$slider_chantiers    = get_field('slider_chantiers');
$slider_realisations = get_field('slider_realisations');


// for other-projects-slider section
$next_posts = get_posts( array(
    'numberposts' => 10,
    'order' => 'ASC',
    'post_status' => 'publish',
    'post_type' => 'project',
    'date_query' => array(
        array(
            'after' => get_post_field( 'post_date', $id ),
            'inclusive' => false,
        ),
    ),
));

if ( empty( $next_posts ) ) {
    $prev_posts = get_posts( array(
        'numberposts' => 10,
        'order' => 'DESC',
        'post_status' => 'publish',
        'post_type' => 'project',
        'date_query' => array(
            array(
                'before' => get_post_field( 'post_date', $id ),
                'inclusive' => false,
            ),
        ),
    ));
}


?>
<?php if ( ! empty ( $title ) ) : ?>
<section id="section-1" class="hero-3 style-bg--black" data-color="black">
    <?php echo load_inline_dependencies('/parts/modules/hero-3/', 'hero-3'); ?>
    <button class="hero-3__back button-back">
        <?php echo get_img( 'arrow-left', '' ); ?>
        <?php echo _e('Retour', 'orfeo'); ?>
    </button>
    <div class="container">
        <div class="hero-3__text">
            <h2 class="hero-3__title h1"><?php echo $title; ?></h2>
            <?php if ( ! empty ( $category_terms ) ) : ?>
            <div class="hero-3__description hero-3__description--uppercase">
                <?php echo $category_terms; ?>
                <?php endif; ?>
                <?php if ( ! empty ( $type_terms ) ) : ?>
                - <?php echo $type_terms; ?>
            </div>
            <?php endif; ?>
            <?php if ( ! empty ( $precise_location ) ) : ?>
            <div class="hero-3__description--small">
                <?php echo $precise_location; ?>
            </div>
            <?php endif; ?>
        </div>
    </div>
    <div class="hero-3__circles rounded-circles-animate rounded-circles-animate-scroll">
        <div class="hero-3__circle hero-3__circle--smaller rounded-circle-animate rounded-circle-animate--smaller">
            <svg width="890" height="890" viewBox="0 0 890 890" fill="none" xmlns="http://www.w3.org/2000/svg">
                <circle cx="445" cy="445" r="444" stroke="white" stroke-opacity="0.3" stroke-width="2" stroke-dasharray="18 18"></circle>
            </svg>
        </div>
        <div class="hero-3__circle hero-3__circle--bigger rounded-circle-animate rounded-circle-animate--bigger">
            <svg width="1300" height="1300" viewBox="0 0 1300 1300" fill="none" xmlns="http://www.w3.org/2000/svg">
                <circle cx="650" cy="650" r="649" stroke="white" stroke-opacity="0.25" stroke-width="2" stroke-dasharray="18 18"></circle>
            </svg>
        </div>
    </div>
</section>
<?php endif; ?>

<?php if ( ! empty ( $subtitle ) && ! empty ( $featured_image ) ) : ?>
<section id="section-2" class="title-with-text-image style-bg--white" data-color="white">
    <?php
    load_blocks_script('title-with-text-image', 'orfeo/' . 'title-with-text-image', '', 'parts/modules');
    echo load_inline_dependencies('/parts/modules/title-with-text-image/', 'title-with-text-image'); ?>
	<div class="title-with-text-image__container container">
	    <div class="title-with-text-image__row">
		    <div class="title-with-text-image__text">
			    <button class="title-with-text-image__button button-back">
                    <?php echo get_img( 'arrow-left', '' ) ?>
    		    </button>
			    <h3 class="title-with-text-image__subtitle section-subtitle"><?php echo _e('Retour aux résultats', 'orfeo'); ?></h3>
                <?php if ( ! empty ( $subtitle ) ) : ?>
			    <h2 class="title-with-text-image__title h1"><?php echo $subtitle; ?></h2>
                <?php endif; ?>
                <?php if ( ! empty ( $description ) ) : ?>
			    <div class="title-with-text-image__description"><?php echo $description; ?></div>
                <?php endif; ?>
		    </div>
            <?php if ( ! empty( $featured_image ) ) : ?>
			<div class="title-with-text-image__image">
				<?php echo $featured_image; ?>
			</div>
		<?php endif; ?>
		</div>
    </div>
</section>
<?php endif; ?>

<?php if ( ! empty( $client ) || ! empty( $delivery_date ) || ! empty( $surface_area ) || ! empty( $architects ) ) : ?>
<section id="section-3" class="title-with-text-boxes style-bg--white" data-color="white">
    <?php
    echo load_inline_dependencies('/parts/modules/title-with-text-boxes/', 'title-with-text-boxes');
    ?>
    <div class="title-with-text-boxes__container container">
	    <div class="title-with-text-boxes__row">
		    <h2 class="title-with-text-boxes__title h1"><i><?php echo _e('Points clés', 'orfeo'); ?></i></h2>
	        <?php if ( ! empty( $client ) || ! empty( $delivery_date ) || ! empty( $surface_area ) || ! empty( $architects ) ) : ?>
		    <ul class="title-with-text-boxes__list title-with-text-boxes__list--four">
                <?php if ( ! empty( $client ) ) : ?>
                <li class="title-with-text-boxes__item title-with-text-boxes__item--client">
                    <?php echo $client; ?>
                </li>
                <?php endif; ?>
                <?php if ( ! empty( $architects ) ) : ?>
                <li class="title-with-text-boxes__item title-with-text-boxes__item--architects">
                    <?php echo $architects; ?>
                </li>
                <?php endif; ?>
                <?php if ( ! empty( $delivery_date ) ) : ?>
                <li class="title-with-text-boxes__item title-with-text-boxes__item--delivery_date">
                    <?php echo $delivery_date; ?>
                </li>
                <?php endif; ?>
                <?php if ( ! empty( $surface_area ) ) : ?>
                <li class="title-with-text-boxes__item title-with-text-boxes__item--surface_area">
                    <?php echo $surface_area; ?>
                </li>
                <?php endif; ?>
                <?php if ( ! empty( $taker ) ) : ?>
                <li class="title-with-text-boxes__item title-with-text-boxes__item--architects">
                    <?php echo $taker; ?>
                </li>
                <?php endif; ?>
		    </ul>
	        <?php endif; ?>
	    </div>
	    <?php if ( ! empty( $certification_terms ) ) : ?>
	    <ul class="title-with-text-boxes__logos">
            <?php echo $certification_terms; ?>
	    </ul>
	    <?php endif; ?>
    </div>
    <div class="title-with-text-boxes__circles title-with-text-boxes__circles--<?php echo $style_bg_color; ?> rounded-circles-animate rounded-circles-animate-scroll">
        <div class="title-with-text-boxes__circle title-with-text-boxes__circle--smaller rounded-circle-animate rounded-circle-animate--smaller">
            <?php if ( $style_bg_color == 'black' ) : ?>
            <svg width="891" height="891" viewBox="0 0 891 891" fill="none" xmlns="http://www.w3.org/2000/svg">
                <circle cx="445.916" cy="445.916" r="444" transform="rotate(-90 445.916 445.916)" stroke="white" stroke-opacity="0.3" stroke-width="2" stroke-dasharray="18 18"/>
            </svg>
            <?php endif; ?>
            <?php if ( $style_bg_color == 'white' ) : ?>
            <svg width="891" height="892" viewBox="0 0 891 892" fill="none" xmlns="http://www.w3.org/2000/svg">
                <circle cx="445.916" cy="446" r="444" stroke="black" stroke-opacity="0.2" stroke-width="2" stroke-dasharray="18 18"/>
            </svg>
            <?php endif; ?>
        </div>
        <div class="title-with-text-boxes__circle title-with-text-boxes__circle--bigger rounded-circle-animate rounded-circle-animate--bigger">
            <?php if ( $style_bg_color == 'black' ) : ?>
            <svg width="1301" height="1301" viewBox="0 0 1301 1301" fill="none" xmlns="http://www.w3.org/2000/svg">
                <circle cx="650.916" cy="650.917" r="649" stroke="white" stroke-opacity="0.3" stroke-width="2" stroke-dasharray="18 18"/>
            </svg>
            <?php endif; ?>
            <?php if ( $style_bg_color == 'white' ) : ?>
            <svg width="1302" height="1302" viewBox="0 0 1302 1302" fill="none" xmlns="http://www.w3.org/2000/svg">
                <circle cx="650.917" cy="651" r="649" stroke="black" stroke-opacity="0.2" stroke-width="2" stroke-dasharray="18 18"/>
            </svg>
            <?php endif; ?>
        </div>
    </div>
</section>
<?php endif; ?>

<?php if ( ! empty ( $title_timeline ) || ! empty ( $description_timeline ) ) : ?>
<section id="section-4" class="realisations style-bg--black" data-color="black">
    <?php echo load_inline_dependencies('/parts/modules/realisations/', 'realisations');
    load_blocks_script( 'realisations', '' . 'realisations', [], 'parts/modules' );
    ?>
    <div class="container">
        <div class="realisations__row realisations__row--block <?php echo empty( $image_timeline ) ? ' realisations__row--no-image' : '' ; ?>">
            <?php if ( ! empty( $image_timeline ) ) : ?>
            <div class="realisations__image">
                <?php echo $image_timeline; ?>
            </div>
            <?php endif; ?>
            <?php if ( ! empty( $image2_timeline ) ) : ?>
            <div class="realisations__video realisations__video--block">
                <?php echo $image2_timeline; ?>
            </div>
            <?php endif; ?>
            <div class="realisations__right realisations__right--block">
                <h3 class="realisations__subtitle section-subtitle"><?php echo _e('Timeline', 'orfeo'); ?></h3>
                <?php if ( ! empty( $title_timeline ) ) : ?>
                <h2 class="realisations__title h2 text-revealup-animate"><?php echo $title_timeline; ?></h2>
                <?php endif; ?>
                <div class="realisations__bottom">
                    <?php if ( ! empty( $description_timeline ) ) : ?>
                    <div class="realisations__description"><?php echo $description_timeline; ?></div>
                    <?php endif; ?>
                </div>
            </div>
        </div>
    </div>
</section>
<?php endif; ?>

<?php if ( ! empty( $step_1 ) || ! empty( $step_2 ) || ! empty( $step_3 ) || ! empty( $step_4 ) ) : ?>
<section id="section-5" class="roadmap style-bg--black" data-color="black">
    <?php echo load_inline_dependencies('/parts/modules/roadmap/', 'roadmap'); ?>
    <div class="roadmap__container container">
        <div class="roadmap__row">
            <ul class="roadmap__list">
                <?php if ( ! empty( $step_1 ) ) : ?>
                <li class="roadmap__item">
                    <div class="roadmap__item__title"><?php echo _e('Étape 1', 'orfeo'); ?></div>
                    <div class="roadmap__item__description">
                        <?php echo $step_1; ?>
                    </div>
                </li>
                <?php endif; ?>
                <?php if ( ! empty( $step_2 ) ) : ?>
                <li class="roadmap__item">
                    <div class="roadmap__item__title"><?php echo _e('Étape 2', 'orfeo'); ?></div>
                    <div class="roadmap__item__description">
                        <?php echo $step_2; ?>
                    </div>
                </li>
                <?php endif; ?>
                <?php if ( ! empty( $step_3 ) ) : ?>
                <li class="roadmap__item">
                    <div class="roadmap__item__title"><?php echo _e('Étape 3', 'orfeo'); ?></div>
                    <div class="roadmap__item__description">
                        <?php echo $step_3; ?>
                    </div>
                </li>
                <?php endif; ?>
                <?php if ( ! empty( $step_4 ) ) : ?>
                <li class="roadmap__item">
                    <div class="roadmap__item__title"><?php echo _e('Étape 4', 'orfeo'); ?></div>
                    <div class="roadmap__item__description">
                        <?php echo $step_4; ?>
                    </div>
                </li>
                <?php endif; ?>
            </ul>
            <div class="roadmap__line"></div>
        </div>
    </div>
</section>
<?php endif; ?>

<?php if ( ! empty ( $main_image ) ) : ?>
<section id="section-6" class="full-screen-image style-bg--black" data-color="black">
    <?php echo load_inline_dependencies('/parts/modules/full-screen-image/', 'full-screen-image'); ?>
    <div class="full-screen-image__image">
	    <?php echo $main_image; ?>
    </div>
</section>
<?php endif; ?>

<?php if ( ! empty( $slider_project_view ) || ! empty( $slider_chantiers ) || ! empty( $slider_realisations ) ) : ?>
<section id="section-7" class="project-single-slider style-bg--white" data-color="white">
    <?php
    load_inline_styles_plugin('splide.min');
    load_inline_styles_shared( 'sliders' );
    load_inline_dependencies('/parts/modules/project-single-slider/', 'project-single-slider');

    load_blocks_script( 'project-single-slider', 'splide/' . 'project-single-slider', ['splide'], 'parts/modules' );

    $number = 0;
    ?>
    <div class="container">
        <h2 class="project-single-slider__title h1"><?php echo _e('Le projet en détail', 'orfeo'); ?></h2>
	    <?php if ( ! empty( $description_slider ) ) : ?>
	    <div class="project-single-slider__description"><?php echo $description_slider; ?></div>
	    <?php endif; ?>
	    <div class="project-single-slider__row">
		    <ul class="project-single-slider__categories">
                <?php if ( ! empty( $slider_project_view ) ) : ?>
                <li class="project-single-slider__category<?php echo ($number + 1 == 1) ? ' active' : ''; ?>" data-tab="<?php echo $number + 1; ?>">
                    <span><?php echo _e('Vue projet', 'orfeo'); ?></span>
                </li>
	            <?php
                $number = $number + 1;
                endif; ?>
                <?php if ( ! empty( $slider_chantiers ) ) : ?>
                <li class="project-single-slider__category<?php echo ($number + 1 == 1) ? ' active' : ''; ?>" data-tab="<?php echo $number + 1; ?>">
                    <span><?php echo _e('Chantiers', 'orfeo'); ?></span>
                </li>
	            <?php
                $number = $number + 1;
                endif; ?>
                <?php if ( ! empty( $slider_realisations ) ) : ?>
                <li class="project-single-slider__category<?php echo ($number + 1 == 1) ? ' active' : ''; ?>" data-tab="<?php echo $number + 1; ?>">
                    <span><?php echo _e('Réalisations', 'orfeo'); ?></span>
                </li>
	            <?php endif;
                $number = 0; ?>
		    </ul>
		    <ul class="project-single-slider__content">
                <?php if ( ! empty( $slider_project_view ) ) : ?>
                <li class="project-single-slider__content-tab<?php echo ($number + 1 == 1) ? ' active' : ''; ?>" data-tab="<?php echo $number + 1; ?>">
                    <div class="project-single-slider__slider splide">
                        <div class="splide__track">
                            <ul class="splide__list">
                            <?php foreach( $slider_project_view as $slide ):
                            $image = wp_get_attachment_image( $slide['image'], 'single-project-slider' );
                            $slide_text = $slide['caption'];
                            ?>
                            <?php if ( ! empty( $image ) ) : ?>
                                <li class="project-single-slider__item splide__slide">
                                    <div class="project-single-slider__item__image"><?php echo $image; ?></div>
                                    <?php if ( ! empty( $slide_text ) ) : ?>
                                    <div class="project-single-slider__item__text"><?php echo $slide_text; ?></div>
                                    <?php endif; ?>
                                </li>
                            <?php endif; ?>
                            <?php endforeach; ?>
                            </ul>
                        </div>
                        <div class="splide__arrows">
                            <button class="splide__arrow splide__arrow--prev">
                                <svg width="41" height="27" viewBox="0 0 41 27" fill="none" xmlns="http://www.w3.org/2000/svg">
                                    <path fill-rule="evenodd" clip-rule="evenodd" d="M40.4043 13.5C40.4043 13.1187 40.2528 12.7531 39.9833 12.4835C39.7137 12.2139 39.348 12.0625 38.9668 12.0625H5.06192L14.1095 3.01774C14.3795 2.74781 14.5311 2.38172 14.5311 1.99998C14.5311 1.61825 14.3795 1.25216 14.1095 0.982234C13.8396 0.71231 13.4735 0.560669 13.0918 0.560669C12.7101 0.560669 12.344 0.71231 12.074 0.982234L0.574051 12.4822C0.440182 12.6158 0.333969 12.7744 0.261501 12.949C0.189034 13.1237 0.15173 13.3109 0.15173 13.5C0.15173 13.6891 0.189034 13.8763 0.261501 14.0509C0.333969 14.2256 0.440182 14.3842 0.574051 14.5177L12.074 26.0177C12.344 26.2877 12.7101 26.4393 13.0918 26.4393C13.4735 26.4393 13.8396 26.2877 14.1095 26.0177C14.3795 25.7478 14.5311 25.3817 14.5311 25C14.5311 24.6183 14.3795 24.2522 14.1095 23.9822L5.06192 14.9375H38.9668C39.348 14.9375 39.7137 14.786 39.9833 14.5165C40.2528 14.2469 40.4043 13.8812 40.4043 13.5Z" fill="currentColor"/>
                                </svg>
                            </button>
                            <button class="splide__arrow splide__arrow--next">
                                <svg width="41" height="27" viewBox="0 0 41 27" fill="none" xmlns="http://www.w3.org/2000/svg">
                                    <path fill-rule="evenodd" clip-rule="evenodd" d="M0.154297 13.5C0.154297 13.1187 0.305747 12.7531 0.575331 12.4835C0.844914 12.2139 1.21055 12.0625 1.5918 12.0625H35.4967L26.449 3.01774C26.1791 2.74781 26.0275 2.38172 26.0275 1.99998C26.0275 1.61825 26.1791 1.25216 26.449 0.982234C26.719 0.71231 27.0851 0.560669 27.4668 0.560669C27.8485 0.560669 28.2146 0.71231 28.4845 0.982234L39.9845 12.4822C40.1184 12.6158 40.2246 12.7744 40.2971 12.949C40.3696 13.1237 40.4069 13.3109 40.4069 13.5C40.4069 13.6891 40.3696 13.8763 40.2971 14.0509C40.2246 14.2256 40.1184 14.3842 39.9845 14.5177L28.4845 26.0177C28.2146 26.2877 27.8485 26.4393 27.4668 26.4393C27.0851 26.4393 26.719 26.2877 26.449 26.0177C26.1791 25.7478 26.0275 25.3817 26.0275 25C26.0275 24.6183 26.1791 24.2522 26.449 23.9822L35.4967 14.9375H1.5918C1.21055 14.9375 0.844914 14.786 0.575331 14.5165C0.305747 14.2469 0.154297 13.8812 0.154297 13.5Z" fill="currentColor"/>
                                </svg>
                            </button>
                        </div>
                    </div>
			    </li>
	            <?php
                $number = $number + 1;
                endif; ?>
                <?php if ( ! empty( $slider_chantiers ) ) : ?>
                <li class="project-single-slider__content-tab<?php echo ($number + 1 == 1) ? ' active' : ''; ?>" data-tab="<?php echo $number + 1; ?>">
                    <div class="project-single-slider__slider splide">
                        <div class="splide__track">
                            <ul class="splide__list">
                            <?php foreach( $slider_chantiers as $slide ):
                            $image = wp_get_attachment_image( $slide['image'], 'single-project-slider' );
                            $slide_text = $slide['caption'];
                            ?>
                            <?php if ( ! empty( $image ) ) : ?>
                                <li class="project-single-slider__item splide__slide">
                                    <div class="project-single-slider__item__image"><?php echo $image; ?></div>
                                    <?php if ( ! empty( $slide_text ) ) : ?>
                                    <div class="project-single-slider__item__text"><?php echo $slide_text; ?></div>
                                    <?php endif; ?>
                                </li>
                            <?php endif; ?>
                            <?php endforeach; ?>
                            </ul>
                        </div>
                        <div class="splide__arrows">
                            <button class="splide__arrow splide__arrow--prev">
                                <svg width="41" height="27" viewBox="0 0 41 27" fill="none" xmlns="http://www.w3.org/2000/svg">
                                    <path fill-rule="evenodd" clip-rule="evenodd" d="M40.4043 13.5C40.4043 13.1187 40.2528 12.7531 39.9833 12.4835C39.7137 12.2139 39.348 12.0625 38.9668 12.0625H5.06192L14.1095 3.01774C14.3795 2.74781 14.5311 2.38172 14.5311 1.99998C14.5311 1.61825 14.3795 1.25216 14.1095 0.982234C13.8396 0.71231 13.4735 0.560669 13.0918 0.560669C12.7101 0.560669 12.344 0.71231 12.074 0.982234L0.574051 12.4822C0.440182 12.6158 0.333969 12.7744 0.261501 12.949C0.189034 13.1237 0.15173 13.3109 0.15173 13.5C0.15173 13.6891 0.189034 13.8763 0.261501 14.0509C0.333969 14.2256 0.440182 14.3842 0.574051 14.5177L12.074 26.0177C12.344 26.2877 12.7101 26.4393 13.0918 26.4393C13.4735 26.4393 13.8396 26.2877 14.1095 26.0177C14.3795 25.7478 14.5311 25.3817 14.5311 25C14.5311 24.6183 14.3795 24.2522 14.1095 23.9822L5.06192 14.9375H38.9668C39.348 14.9375 39.7137 14.786 39.9833 14.5165C40.2528 14.2469 40.4043 13.8812 40.4043 13.5Z" fill="currentColor"/>
                                </svg>
                            </button>
                            <button class="splide__arrow splide__arrow--next">
                                <svg width="41" height="27" viewBox="0 0 41 27" fill="none" xmlns="http://www.w3.org/2000/svg">
                                    <path fill-rule="evenodd" clip-rule="evenodd" d="M0.154297 13.5C0.154297 13.1187 0.305747 12.7531 0.575331 12.4835C0.844914 12.2139 1.21055 12.0625 1.5918 12.0625H35.4967L26.449 3.01774C26.1791 2.74781 26.0275 2.38172 26.0275 1.99998C26.0275 1.61825 26.1791 1.25216 26.449 0.982234C26.719 0.71231 27.0851 0.560669 27.4668 0.560669C27.8485 0.560669 28.2146 0.71231 28.4845 0.982234L39.9845 12.4822C40.1184 12.6158 40.2246 12.7744 40.2971 12.949C40.3696 13.1237 40.4069 13.3109 40.4069 13.5C40.4069 13.6891 40.3696 13.8763 40.2971 14.0509C40.2246 14.2256 40.1184 14.3842 39.9845 14.5177L28.4845 26.0177C28.2146 26.2877 27.8485 26.4393 27.4668 26.4393C27.0851 26.4393 26.719 26.2877 26.449 26.0177C26.1791 25.7478 26.0275 25.3817 26.0275 25C26.0275 24.6183 26.1791 24.2522 26.449 23.9822L35.4967 14.9375H1.5918C1.21055 14.9375 0.844914 14.786 0.575331 14.5165C0.305747 14.2469 0.154297 13.8812 0.154297 13.5Z" fill="currentColor"/>
                                </svg>
                            </button>
                        </div>
                    </div>
			    </li>
	            <?php
                $number = $number + 1;
                endif; ?>
                <?php if ( ! empty( $slider_realisations ) ) : ?>
                <li class="project-single-slider__content-tab<?php echo ($number + 1 == 1) ? ' active' : ''; ?>" data-tab="<?php echo $number + 1; ?>">
                    <div class="project-single-slider__slider splide">
                        <div class="splide__track">
                            <ul class="splide__list">
                            <?php foreach( $slider_realisations as $slide ):
                            $image = wp_get_attachment_image( $slide['image'], 'single-project-slider' );
                            $slide_text = $slide['caption'];
                            ?>
                            <?php if ( ! empty( $image ) ) : ?>
                                <li class="project-single-slider__item splide__slide">
                                    <div class="project-single-slider__item__image"><?php echo $image; ?></div>
                                    <?php if ( ! empty( $slide_text ) ) : ?>
                                    <div class="project-single-slider__item__text"><?php echo $slide_text; ?></div>
                                    <?php endif; ?>
                                </li>
                            <?php endif; ?>
                            <?php endforeach; ?>
                            </ul>
                        </div>
                        <div class="splide__arrows">
                            <button class="splide__arrow splide__arrow--prev">
                                <svg width="41" height="27" viewBox="0 0 41 27" fill="none" xmlns="http://www.w3.org/2000/svg">
                                    <path fill-rule="evenodd" clip-rule="evenodd" d="M40.4043 13.5C40.4043 13.1187 40.2528 12.7531 39.9833 12.4835C39.7137 12.2139 39.348 12.0625 38.9668 12.0625H5.06192L14.1095 3.01774C14.3795 2.74781 14.5311 2.38172 14.5311 1.99998C14.5311 1.61825 14.3795 1.25216 14.1095 0.982234C13.8396 0.71231 13.4735 0.560669 13.0918 0.560669C12.7101 0.560669 12.344 0.71231 12.074 0.982234L0.574051 12.4822C0.440182 12.6158 0.333969 12.7744 0.261501 12.949C0.189034 13.1237 0.15173 13.3109 0.15173 13.5C0.15173 13.6891 0.189034 13.8763 0.261501 14.0509C0.333969 14.2256 0.440182 14.3842 0.574051 14.5177L12.074 26.0177C12.344 26.2877 12.7101 26.4393 13.0918 26.4393C13.4735 26.4393 13.8396 26.2877 14.1095 26.0177C14.3795 25.7478 14.5311 25.3817 14.5311 25C14.5311 24.6183 14.3795 24.2522 14.1095 23.9822L5.06192 14.9375H38.9668C39.348 14.9375 39.7137 14.786 39.9833 14.5165C40.2528 14.2469 40.4043 13.8812 40.4043 13.5Z" fill="currentColor"/>
                                </svg>
                            </button>
                            <button class="splide__arrow splide__arrow--next">
                                <svg width="41" height="27" viewBox="0 0 41 27" fill="none" xmlns="http://www.w3.org/2000/svg">
                                    <path fill-rule="evenodd" clip-rule="evenodd" d="M0.154297 13.5C0.154297 13.1187 0.305747 12.7531 0.575331 12.4835C0.844914 12.2139 1.21055 12.0625 1.5918 12.0625H35.4967L26.449 3.01774C26.1791 2.74781 26.0275 2.38172 26.0275 1.99998C26.0275 1.61825 26.1791 1.25216 26.449 0.982234C26.719 0.71231 27.0851 0.560669 27.4668 0.560669C27.8485 0.560669 28.2146 0.71231 28.4845 0.982234L39.9845 12.4822C40.1184 12.6158 40.2246 12.7744 40.2971 12.949C40.3696 13.1237 40.4069 13.3109 40.4069 13.5C40.4069 13.6891 40.3696 13.8763 40.2971 14.0509C40.2246 14.2256 40.1184 14.3842 39.9845 14.5177L28.4845 26.0177C28.2146 26.2877 27.8485 26.4393 27.4668 26.4393C27.0851 26.4393 26.719 26.2877 26.449 26.0177C26.1791 25.7478 26.0275 25.3817 26.0275 25C26.0275 24.6183 26.1791 24.2522 26.449 23.9822L35.4967 14.9375H1.5918C1.21055 14.9375 0.844914 14.786 0.575331 14.5165C0.305747 14.2469 0.154297 13.8812 0.154297 13.5Z" fill="currentColor"/>
                                </svg>
                            </button>
                        </div>
                    </div>
			    </li>
	            <?php endif; ?>
		    </ul>
	    </div>
    </div>
</section>
<?php endif; ?>

<section id="section-8" class="other-projects-slider style-bg--black" data-color="black">
<?php
    load_inline_styles_plugin('splide.min');
    load_inline_styles_shared( 'sliders' );
    load_inline_dependencies('/parts/modules/other-projects-slider/', 'other-projects-slider');

    load_blocks_script( 'other-projects-slider', 'splide/' . 'other-projects-slider', ['splide'], 'parts/modules' );
    ?>

    <div class="container">
	    <div class="other-projects-slider__row">
		<h2 class="other-projects-slider__title h1"><?php echo _e('Ces projets peuvent vous intéresser', 'orfeo'); ?></h2>
		<div class="other-projects-slider__slider splide">
			<div class="splide__track">
				<ul class="splide__list">
				<?php
                $projects = $next_posts;
                if ( empty ($next_posts)) :
                    $projects = $prev_posts;
                endif;

                foreach( $projects as $post ):
				setup_postdata($post);
				$title       = get_the_title( $post );
				$post_link   = get_permalink( $post );
				$image       = get_the_post_thumbnail( $post );

				$category = get_the_terms( $post, 'project-category' );
				$category_terms = '';

				if ( $category && ! is_wp_error( $category ) ) :
					foreach ( $category as $key => $term ) :
						if ($key !== array_key_first($category)) :
							$category_terms .= ' - ';
						endif;
						$category_terms .= $term->name;
					endforeach;
				endif;
				?>
					<li class="other-projects-slider__item splide__slide">
						<a class="other-projects-slider__item__image" href="<?php echo $post_link; ?>"><?php echo $image; ?></a>
						<h2 class="other-projects-slider__item__title h3"><a href="<?php echo $post_link; ?>"><?php echo $title; ?></a></h2>
						<?php if ( !empty( $category_terms ) ) : ?>
						<div class="other-projects-slider__item__categories"><?php echo $category_terms; ?></div>
						<a class="other-projects-slider__item__link" href="<?php echo $post_link; ?>"><?php echo _e('En savoir +', 'orfeo'); ?></a>
						<?php endif; ?>
					</li>
				<?php endforeach;
				wp_reset_postdata();
				?>
				</ul>
			</div>
			<div class="splide__arrows">
				<button class="splide__arrow splide__arrow--prev">
					<svg width="41" height="27" viewBox="0 0 41 27" fill="none" xmlns="http://www.w3.org/2000/svg">
						<path fill-rule="evenodd" clip-rule="evenodd" d="M40.4043 13.5C40.4043 13.1187 40.2528 12.7531 39.9833 12.4835C39.7137 12.2139 39.348 12.0625 38.9668 12.0625H5.06192L14.1095 3.01774C14.3795 2.74781 14.5311 2.38172 14.5311 1.99998C14.5311 1.61825 14.3795 1.25216 14.1095 0.982234C13.8396 0.71231 13.4735 0.560669 13.0918 0.560669C12.7101 0.560669 12.344 0.71231 12.074 0.982234L0.574051 12.4822C0.440182 12.6158 0.333969 12.7744 0.261501 12.949C0.189034 13.1237 0.15173 13.3109 0.15173 13.5C0.15173 13.6891 0.189034 13.8763 0.261501 14.0509C0.333969 14.2256 0.440182 14.3842 0.574051 14.5177L12.074 26.0177C12.344 26.2877 12.7101 26.4393 13.0918 26.4393C13.4735 26.4393 13.8396 26.2877 14.1095 26.0177C14.3795 25.7478 14.5311 25.3817 14.5311 25C14.5311 24.6183 14.3795 24.2522 14.1095 23.9822L5.06192 14.9375H38.9668C39.348 14.9375 39.7137 14.786 39.9833 14.5165C40.2528 14.2469 40.4043 13.8812 40.4043 13.5Z" fill="currentColor"/>
					</svg>
				</button>
				<button class="splide__arrow splide__arrow--next">
					<svg width="41" height="27" viewBox="0 0 41 27" fill="none" xmlns="http://www.w3.org/2000/svg">
						<path fill-rule="evenodd" clip-rule="evenodd" d="M0.154297 13.5C0.154297 13.1187 0.305747 12.7531 0.575331 12.4835C0.844914 12.2139 1.21055 12.0625 1.5918 12.0625H35.4967L26.449 3.01774C26.1791 2.74781 26.0275 2.38172 26.0275 1.99998C26.0275 1.61825 26.1791 1.25216 26.449 0.982234C26.719 0.71231 27.0851 0.560669 27.4668 0.560669C27.8485 0.560669 28.2146 0.71231 28.4845 0.982234L39.9845 12.4822C40.1184 12.6158 40.2246 12.7744 40.2971 12.949C40.3696 13.1237 40.4069 13.3109 40.4069 13.5C40.4069 13.6891 40.3696 13.8763 40.2971 14.0509C40.2246 14.2256 40.1184 14.3842 39.9845 14.5177L28.4845 26.0177C28.2146 26.2877 27.8485 26.4393 27.4668 26.4393C27.0851 26.4393 26.719 26.2877 26.449 26.0177C26.1791 25.7478 26.0275 25.3817 26.0275 25C26.0275 24.6183 26.1791 24.2522 26.449 23.9822L35.4967 14.9375H1.5918C1.21055 14.9375 0.844914 14.786 0.575331 14.5165C0.305747 14.2469 0.154297 13.8812 0.154297 13.5Z" fill="currentColor"/>
					</svg>
				</button>
			</div>
		</div>
		<?php if ( !empty( $link ) ) : ?>
			<div class="other-projects-slider__link"><?php echo get_button( $link , '', '', 'icon-dot' ); ?></div>
		<?php endif; ?>
	</div>
</div>
</div>
</section>

