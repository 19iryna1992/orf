<?php
/**
 * Theme Header
 *
 * @package    WordPress
 * @subpackage orfeo
 * @since      orfeo 1.0
 */

$output_scripts = ( ( substr( home_url(), -4 ) === '/web' ) || current_user_can( 'administrator' ) ) ? false : true;
?>
<!DOCTYPE html>
<html <?php language_attributes(); ?> class="no-js">
<head>
	<meta charset="UTF-8"/>
	<meta name="viewport" content="width=device-width, initial-scale=1.0"/>
	<?php wp_head(); ?>
	<?php if ( $output_scripts ) the_field( 'header_html', 'options' ); ?>
</head>
<body <?php body_class(); ?>>
	<?php if ( $output_scripts ) the_field( 'body_html', 'options' ); ?>
	<?php wp_body_open(); ?>
	<div id="page">
		<a class="skip-link" href="#content"><?php esc_html_e( 'Skip to content', 'orfeo' ); ?></a>
		<?php echo get_part('components/header/index'); ?>
