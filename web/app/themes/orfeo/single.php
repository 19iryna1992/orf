<?php
/**
 * Single Blog Post template.
 *
 * @package    WordPress
 * @subpackage orfeo
 * @since      orfeo 1.0
 */
get_header();
the_post();

$type = get_post_type();
?>
<main id="page-content" role="main" class="page-content page-content--single">
	<h1 class="page-title screen-reader-text"><?php the_title(); ?></h1>
	<div id="content" tabindex="-1" class="page-content__wrapper">
	<?php
	if ( $type === 'project' ) :
		get_part('components/project-single/index');
	endif;
	?>
</main>
<?php
get_footer();
