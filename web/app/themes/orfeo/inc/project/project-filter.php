<?php
add_action('wp_ajax_project_filter', 'project_filter');
add_action('wp_ajax_nopriv_project_filter', 'project_filter');

function project_filter()
{

    $location = $_POST['location'] ? explode(',', $_POST['location']) : NULL;
    $cat = $_POST['cat'] ? explode(',', $_POST['cat']) : NULL;
    $type = $_POST['type'] ? explode(',', $_POST['type']) : NULL;
    $count = 1;

    $posts_args = [
        'post_type' => 'project',
        'post_status' => 'publish',
        'posts_per_page' => 8,

    ];

    if ($location || $cat || $type) {
        $posts_args['tax_query'] = array(
            'relation' => 'OR',
        );
    }

    if (!empty($location)) {
        $posts_args['tax_query'][] = array(
            'taxonomy' => 'project-location',
            'field' => 'term_id',
            'terms' => $location,
        );
    }

    if (!empty($cat)) {
        $posts_args['tax_query'][] = array(
            'taxonomy' => 'project-category',
            'field' => 'term_id',
            'terms' => $cat,
        );
    }

    if (isset($type)) {
        $posts_args['tax_query'][] = array(
            'taxonomy' => 'project-type',
            'field' => 'term_id',
            'terms' => $type,
        );
    }

	$output      = '';
	$posts_query = new WP_Query( $posts_args );

    $posts_query_posts = $posts_query->posts;
    $post_not_in = [];

    foreach( $posts_query_posts as $posts_query_post) {
        array_push($post_not_in, $posts_query_post->ID);
    }

    $temp_args = array(
        'post_type'      => 'project',
        'posts_per_page' => 8,
        'post_status'    => 'publish',
        'post__not_in'   => $post_not_in
    );

    if ($location || $cat || $type) {
        $temp_args['tax_query'] = array(
            'relation' => 'OR',
        );
    }

    if (!empty($location)) {
        $temp_args['tax_query'][] = array(
            'taxonomy' => 'project-location',
            'field' => 'term_id',
            'terms' => $location,
        );
    }

    if (!empty($cat)) {
        $temp_args['tax_query'][] = array(
            'taxonomy' => 'project-category',
            'field' => 'term_id',
            'terms' => $cat,
        );
    }

    if (isset($type)) {
        $temp_args['tax_query'][] = array(
            'taxonomy' => 'project-type',
            'field' => 'term_id',
            'terms' => $type,
        );
    }

    $temp_posts_query = new WP_Query( $temp_args );
    $temp_posts_found = $temp_posts_query->found_posts;

	$posts_found = ( $temp_posts_found == 0 ) ? $temp_posts_found : $posts_query->found_posts;

    $exclude_filter = '';

    if ($posts_query->have_posts()) :

        while ($posts_query->have_posts()) :
            ob_start();

            $posts_query->the_post();

            $project_id = get_the_ID();
            $project_cat = get_the_terms($project_id, 'project-category');
            $project_type = get_the_terms($project_id, 'project-type');
            $precise_location = get_field('precise_location', $project_id);
            $article_class = '';

            if ( ! empty($exclude_filter) ) {
                $exclude_filter = $exclude_filter . ',';
            }

            $exclude_filter = $exclude_filter . $project_id;

            if ($count % 2 == 0) :
                $article_class = "post-grid__articles--right";
            else :
                $article_class = "post-grid__articles--left";
            endif;

            $output .= '<div class="post-grid__articles ' . $article_class . ' col-12 col-lg-6">';
            if ( !empty ( $precise_location ) ) :
                $output .= '<div class="post-grid__articles-location">';
                $output .= '<span class="">' . $precise_location . '</span>';
                $output .= '</div>';
            endif;
            $output .= '<a class="post-grid__articles-thumbnail" href="' . get_permalink() . '">';
            $output .= get_the_post_thumbnail($project_id, array('700', '500'));
            $output .= '</a>';
            $output .= '<div class="post-grid__articles-info">';
            $output .= '<div class="post-grid__articles-title">' . get_the_title() . '</div>';
            $output .= '<span class="post-grid__articles-details">';
                if ($project_cat && !is_wp_error($project_cat)) :
                    foreach ($project_cat as $term) :
                        $output .= '<span class="post-grid__"> ' . $term->name . ' </span>';
                    endforeach;
                endif;
                if ($project_type && !is_wp_error($project_type) && $project_cat && !is_wp_error($project_cat)) :
                    $output .= '-';
                endif;
                if ($project_type && !is_wp_error($project_type)) :
                    foreach ($project_type as $term) :
                        $output .= '<span class="post-grid__"> ' . $term->name . ' </span>';
                    endforeach;
                endif;
            $output .= '</span>';
            $output .= '</div>';
            $output .= '<div class="post-grid__articles-permalink">';
            $output .= '<a href="' . get_permalink() . '">' . __('En savoir ', 'orfeo') . '</a>';
            $output .= '</div>';
            $output .= '</div>';
            $output .= ob_get_clean();

            ++$count;
        endwhile;
        wp_reset_postdata();
    endif;

	$count = ($posts_found == 0 ) ? 0 : $temp_posts_found;

	echo json_encode( [
		'output' => $output,
		'count'  => $count,
        'excludeFilter'  => $exclude_filter,
	] );

	die();
}



function project_show_more() {
	$offset        = ! empty( $_POST['offset'] ) ? intval( $_POST['offset'] ) : 0;
	$exclude       = ! empty( $_POST['exclude'] ) ? wp_parse_id_list( $_POST['exclude'] ) : [];
	$per_page      = ! empty( $_POST['per_page'] ) ? intval( $_POST['per_page'] ) : 0;

    $location = $_POST['location'] ? explode(',', $_POST['location']) : NULL;
    $cat = $_POST['cat'] ? explode(',', $_POST['cat']) : NULL;
    $type = $_POST['type'] ? explode(',', $_POST['type']) : NULL;

	$temp_posts_found = 0;

	$include = $exclude;

	$args = array(
		'post_type'      => 'project',
		'offset'         => $offset,
		'posts_per_page' => $per_page,
		'post_status'    => 'publish',
	);

	if ( ! empty( $exclude ) ) {
		$args['post__not_in'] = $exclude;
	}

    if ($location || $cat || $type) {
        $args['tax_query'] = array(
            'relation' => 'OR',
        );
    }

    if (!empty($location)) {
        $args['tax_query'][] = array(
            'taxonomy' => 'project-location',
            'field' => 'term_id',
            'terms' => $location,
        );
    }

    if (!empty($cat)) {
        $args['tax_query'][] = array(
            'taxonomy' => 'project-category',
            'field' => 'term_id',
            'terms' => $cat,
        );
    }

    if (isset($type)) {
        $args['tax_query'][] = array(
            'taxonomy' => 'project-type',
            'field' => 'term_id',
            'terms' => $type,
        );
    }

	$temp_args = array(
		'post_type'      => 'project',
		'posts_per_page' => $per_page,
		'post_status'    => 'publish',
		'post__not_in'   => $include
    );

    if ($location || $cat || $type) {
        $temp_args['tax_query'] = array(
            'relation' => 'OR',
        );
    }

    if (!empty($location)) {
        $temp_args['tax_query'][] = array(
            'taxonomy' => 'project-location',
            'field' => 'term_id',
            'terms' => $location,
        );
    }

    if (!empty($cat)) {
        $temp_args['tax_query'][] = array(
            'taxonomy' => 'project-category',
            'field' => 'term_id',
            'terms' => $cat,
        );
    }

    if (isset($type)) {
        $temp_args['tax_query'][] = array(
            'taxonomy' => 'project-type',
            'field' => 'term_id',
            'terms' => $type,
        );
    }

	$temp_posts_query = new WP_Query( $temp_args );
	$temp_posts_found = $temp_posts_query->found_posts;

	$output      = '';
	$posts_query = new WP_Query( $args );
	$posts_found = ! empty( $temp_posts_found ) ? $temp_posts_found : $posts_query->found_posts;

    $count = 1;

    if ($posts_query->have_posts()) :

        while ($posts_query->have_posts()) :
            ob_start();

            $posts_query->the_post();

            $project_id = get_the_ID();
            $project_cat = get_the_terms($project_id, 'project-category');
            $project_type = get_the_terms($project_id, 'project-type');
            $precise_location = get_field('precise_location', $project_id);
            $article_class = '';

            if ($count % 2 == 0) :
                $article_class = "post-grid__articles--right";
            else :
                $article_class = "post-grid__articles--left";
            endif;

            $output .= '<div class="post-grid__articles ' . $article_class . ' col-12 col-lg-6">';
            if ( !empty ( $precise_location ) ) :
                $output .= '<div class="post-grid__articles-location">';
                $output .= '<span class="">' . $precise_location . '</span>';
                $output .= '</div>';
            endif;
            $output .= '<a class="post-grid__articles-thumbnail" href="' . get_permalink() . '">';
            $output .= get_the_post_thumbnail($project_id, array('700', '500'));
            $output .= '</a>';
            $output .= '<div class="post-grid__articles-info">';
            $output .= '<div class="post-grid__articles-title">' . get_the_title() . '</div>';
            $output .= '<span class="post-grid__articles-details">';
                if ($project_cat && !is_wp_error($project_cat)) :
                    foreach ($project_cat as $term) :
                        $output .= '<span class="post-grid__"> ' . $term->name . ' </span>';
                    endforeach;
                endif;
                if ($project_type && !is_wp_error($project_type) && $project_cat && !is_wp_error($project_cat)) :
                    $output .= '-';
                endif;
                if ($project_type && !is_wp_error($project_type)) :
                    foreach ($project_type as $term) :
                        $output .= '<span class="post-grid__"> ' . $term->name . ' </span>';
                    endforeach;
                endif;
            $output .= '</span>';
            $output .= '</div>';
            $output .= '<div class="post-grid__articles-permalink">';
            $output .= '<a href="' . get_permalink() . '">' . __('En savoir ', 'orfeo') . '</a>';
            $output .= '</div>';
            $output .= '</div>';
            $output .= ob_get_clean();

            ++$count;
        endwhile;
        wp_reset_postdata();
    endif;

	$offset += $per_page;
	$count = ( ($posts_found - $offset) <= 0 ) ? 0 : ($posts_found - $offset);

	echo json_encode( [
		'output' => $output,
		'offset' => $offset,
		'count'  => $count,
	] );

	die();
}
add_action('wp_ajax_project_show_more', 'project_show_more');
add_action('wp_ajax_nopriv_project_show_more', 'project_show_more');
