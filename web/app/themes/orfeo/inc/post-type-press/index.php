<?php

/**
 * Register a custom post type called "projects".
 *
 * @see get_post_type_labels() for label keys.
 */
function wpdocs_codex_press_init() {
	$labels = array(
		'name'                  => _x( 'Press', 'Post type general name', 'orfeo' ),
		'singular_name'         => _x( 'Press', 'Post type singular name', 'orfeo' ),
		'origin_name'           => _x( 'Press', 'Post type origin name', 'orfeo' ),
		'menu_name'             => _x( 'Press', 'Admin Menu text', 'orfeo' ),
		'name_admin_bar'        => _x( 'Press', 'Add New on Toolbar', 'orfeo' ),
		'add_new'               => __( 'Add New', 'orfeo' ),
		'add_new_item'          => __( 'Add New Press Item', 'orfeo' ),
		'new_item'              => __( 'New Press Item', 'orfeo' ),
		'edit_item'             => __( 'Edit Press Item', 'orfeo' ),
		'view_item'             => __( 'View Press Item', 'orfeo' ),
		'all_items'             => __( 'All Press', 'orfeo' ),
		'search_items'          => __( 'Search Press', 'orfeo' ),
		'parent_item_colon'     => __( 'Parent Press:', 'orfeo' ),
		'not_found'             => __( 'No news found.', 'orfeo' ),
		'not_found_in_trash'    => __( 'No news found in Trash.', 'orfeo' ),
		'featured_image'        => _x( 'Press Image', 'Overrides the “Featured Image” phrase for this post type. Added in 4.3', 'orfeo' ),
		'set_featured_image'    => _x( 'Set image', 'Overrides the “Set featured image” phrase for this post type. Added in 4.3', 'orfeo' ),
		'remove_featured_image' => _x( 'Remove image', 'Overrides the “Remove featured image” phrase for this post type. Added in 4.3', 'orfeo' ),
		'use_featured_image'    => _x( 'Use as image', 'Overrides the “Use as featured image” phrase for this post type. Added in 4.3', 'orfeo' ),
		'archives'              => _x( 'Press archives', 'The post type archive label used in nav menus. Default “Post Archives”. Added in 4.4', 'orfeo' ),
		'insert_into_item'      => _x( 'Insert into press', 'Overrides the “Insert into post”/”Insert into page” phrase (used when inserting media into a press). Added in 4.4', 'orfeo' ),
		'uploaded_to_this_item' => _x( 'Uploaded to this project', 'Overrides the “Uploaded to this post”/”Uploaded to this page” phrase (used when viewing media attached to a post). Added in 4.4', 'orfeo' ),
		'filter_items_list'     => _x( 'Filter Press list', 'Screen reader text for the filter links heading on the post type listing screen. Default “Filter posts list”/”Filter pages list”. Added in 4.4', 'orfeo' ),
		'items_list_navigation' => _x( 'Press list navigation', 'Screen reader text for the pagination heading on the post type listing screen. Default “Posts list navigation”/”Pages list navigation”. Added in 4.4', 'orfeo' ),
		'items_list'            => _x( 'Press list', 'Screen reader text for the items list heading on the post type listing screen. Default “Posts list”/”Pages list”. Added in 4.4', 'orfeo' ),
	);

	$args = array(
		'labels'             => $labels,
		'public'             => true,
		'publicly_queryable' => false,
		'show_ui'            => true,
		'show_in_menu'       => true,
		'query_var'          => true,
		'rewrite'            => array( 'slug' => 'press' ),
		'capability_type'    => 'post',
		'has_archive'        => false,
		'hierarchical'       => false,
		'menu_position'      => null,
		'supports'           => array( 'title', 'thumbnail' ),
		'menu_icon'         => 'dashicons-format-aside'
	);

	register_post_type( 'press', $args );
}

add_action( 'init', 'wpdocs_codex_press_init' );
