<?php

/**
 * Register a custom post type called "projects".
 *
 * @see get_post_type_labels() for label keys.
 */
function wpdocs_codex_team_init() {
	$labels = array(
		'name'                  => _x( 'Team', 'Post type general name', 'orfeo' ),
		'singular_name'         => _x( 'Team', 'Post type singular name', 'orfeo' ),
		'origin_name'           => _x( 'Team', 'Post type origin name', 'orfeo' ),
		'menu_name'             => _x( 'Team', 'Admin Menu text', 'orfeo' ),
		'name_admin_bar'        => _x( 'Team', 'Add New on Toolbar', 'orfeo' ),
		'add_new'               => __( 'Add New', 'orfeo' ),
		'add_new_item'          => __( 'Add New Team Member', 'orfeo' ),
		'new_item'              => __( 'New Team Member', 'orfeo' ),
		'edit_item'             => __( 'Edit Team Member', 'orfeo' ),
		'view_item'             => __( 'View Team Member', 'orfeo' ),
		'all_items'             => __( 'All Team', 'orfeo' ),
		'search_items'          => __( 'Search Team', 'orfeo' ),
		'parent_item_colon'     => __( 'Parent Team Member:', 'orfeo' ),
		'not_found'             => __( 'No news found.', 'orfeo' ),
		'not_found_in_trash'    => __( 'No news found in Trash.', 'orfeo' ),
		'featured_image'        => _x( 'Team Member Image', 'Overrides the “Featured Image” phrase for this post type. Added in 4.3', 'orfeo' ),
		'set_featured_image'    => _x( 'Set image', 'Overrides the “Set featured image” phrase for this post type. Added in 4.3', 'orfeo' ),
		'remove_featured_image' => _x( 'Remove image', 'Overrides the “Remove featured image” phrase for this post type. Added in 4.3', 'orfeo' ),
		'use_featured_image'    => _x( 'Use as image', 'Overrides the “Use as featured image” phrase for this post type. Added in 4.3', 'orfeo' ),
		'archives'              => _x( 'Team archives', 'The post type archive label used in nav menus. Default “Post Archives”. Added in 4.4', 'orfeo' ),
		'insert_into_item'      => _x( 'Insert into team member', 'Overrides the “Insert into post”/”Insert into page” phrase (used when inserting media into a post). Added in 4.4', 'orfeo' ),
		'uploaded_to_this_item' => _x( 'Uploaded to this team member', 'Overrides the “Uploaded to this post”/”Uploaded to this page” phrase (used when viewing media attached to a post). Added in 4.4', 'orfeo' ),
		'filter_items_list'     => _x( 'Filter Team list', 'Screen reader text for the filter links heading on the post type listing screen. Default “Filter posts list”/”Filter pages list”. Added in 4.4', 'orfeo' ),
		'items_list_navigation' => _x( 'Team list navigation', 'Screen reader text for the pagination heading on the post type listing screen. Default “Posts list navigation”/”Pages list navigation”. Added in 4.4', 'orfeo' ),
		'items_list'            => _x( 'Team list', 'Screen reader text for the items list heading on the post type listing screen. Default “Posts list”/”Pages list”. Added in 4.4', 'orfeo' ),
	);

	$args = array(
		'labels'             => $labels,
		'public'             => true,
		'publicly_queryable' => false,
		'show_ui'            => true,
		'show_in_menu'       => true,
		'query_var'          => true,
		'rewrite'            => array( 'slug' => 'team' ),
		'capability_type'    => 'post',
		'has_archive'        => false,
		'hierarchical'       => false,
		'menu_position'      => null,
		'supports'           => array( 'title', 'thumbnail' ),
		'menu_icon'         => 'dashicons-admin-users'
	);

	register_post_type( 'team', $args );
}

add_action( 'init', 'wpdocs_codex_team_init' );
