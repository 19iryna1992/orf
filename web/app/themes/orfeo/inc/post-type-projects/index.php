<?php

/**
 * Register a custom post type called "projects".
 *
 * @see get_post_type_labels() for label keys.
 */
function wpdocs_codex_project_init() {
	$labels = array(
		'name'                  => _x( 'Project', 'Post type general name', 'orfeo' ),
		'singular_name'         => _x( 'Project', 'Post type singular name', 'orfeo' ),
		'origin_name'           => _x( 'Projets', 'Post type origin name', 'orfeo' ),
		'menu_name'             => _x( 'Projects', 'Admin Menu text', 'orfeo' ),
		'name_admin_bar'        => _x( 'Projects', 'Add New on Toolbar', 'orfeo' ),
		'add_new'               => __( 'Add New', 'orfeo' ),
		'add_new_item'          => __( 'Add New Project', 'orfeo' ),
		'new_item'              => __( 'New Project', 'orfeo' ),
		'edit_item'             => __( 'Edit Project', 'orfeo' ),
		'view_item'             => __( 'View Project', 'orfeo' ),
		'all_items'             => __( 'All Projects', 'orfeo' ),
		'search_items'          => __( 'Search Projects', 'orfeo' ),
		'parent_item_colon'     => __( 'Parent Project:', 'orfeo' ),
		'not_found'             => __( 'No news found.', 'orfeo' ),
		'not_found_in_trash'    => __( 'No news found in Trash.', 'orfeo' ),
		'featured_image'        => _x( 'Project Image', 'Overrides the “Featured Image” phrase for this post type. Added in 4.3', 'orfeo' ),
		'set_featured_image'    => _x( 'Set image', 'Overrides the “Set featured image” phrase for this post type. Added in 4.3', 'orfeo' ),
		'remove_featured_image' => _x( 'Remove image', 'Overrides the “Remove featured image” phrase for this post type. Added in 4.3', 'orfeo' ),
		'use_featured_image'    => _x( 'Use as image', 'Overrides the “Use as featured image” phrase for this post type. Added in 4.3', 'orfeo' ),
		'archives'              => _x( 'Project archives', 'The post type archive label used in nav menus. Default “Post Archives”. Added in 4.4', 'orfeo' ),
		'insert_into_item'      => _x( 'Insert into project', 'Overrides the “Insert into post”/”Insert into page” phrase (used when inserting media into a post). Added in 4.4', 'orfeo' ),
		'uploaded_to_this_item' => _x( 'Uploaded to this project', 'Overrides the “Uploaded to this post”/”Uploaded to this page” phrase (used when viewing media attached to a post). Added in 4.4', 'orfeo' ),
		'filter_items_list'     => _x( 'Filter Project list', 'Screen reader text for the filter links heading on the post type listing screen. Default “Filter posts list”/”Filter pages list”. Added in 4.4', 'orfeo' ),
		'items_list_navigation' => _x( 'Project list navigation', 'Screen reader text for the pagination heading on the post type listing screen. Default “Posts list navigation”/”Pages list navigation”. Added in 4.4', 'orfeo' ),
		'items_list'            => _x( 'Project list', 'Screen reader text for the items list heading on the post type listing screen. Default “Posts list”/”Pages list”. Added in 4.4', 'orfeo' ),
	);

	$args = array(
		'labels'             => $labels,
		'public'             => true,
		'publicly_queryable' => true,
		'show_ui'            => true,
		'show_in_menu'       => true,
		'query_var'          => true,
		'rewrite'            => array( 'slug' => 'project' ),
		'capability_type'    => 'post',
		'has_archive'        => false,
		'hierarchical'       => false,
		'menu_position'      => null,
		'supports'           => array( 'title', 'thumbnail' ),
		'menu_icon'         => 'dashicons-paperclip'
	);

	register_post_type( 'project', $args );
}

add_action( 'init', 'wpdocs_codex_project_init' );


/**
 * Create four taxonomies for job post type called office department contract location
 *
 * @see register_post_type() for registering custom post types.
 */
function orfeo_create_project_taxonomies() {
	$labels = array(
		'name'              => _x( 'Type', 'taxonomy general name', 'orfeo' ),
		'singular_name'     => _x( 'Type', 'taxonomy singular name', 'orfeo' ),
		'search_items'      => __( 'Search Type', 'orfeo' ),
		'all_items'         => __( 'All Types', 'orfeo' ),
		'parent_item'       => __( 'Parent Type', 'orfeo' ),
		'parent_item_colon' => __( 'Parent Type:', 'orfeo' ),
		'edit_item'         => __( 'Edit Type', 'orfeo' ),
		'update_item'       => __( 'Update Type', 'orfeo' ),
		'add_new_item'      => __( 'Add New Type', 'orfeo' ),
		'new_item_name'     => __( 'New Type Name', 'orfeo' ),
		'menu_name'         => __( 'Type', 'orfeo' ),
	);

	$args = array(
		'hierarchical'      => true,
		'labels'            => $labels,
		'show_ui'           => true,
		'show_admin_column' => true,
		'query_var'         => true,
		'rewrite'           => array( 'slug' => 'type' ),
	);

	register_taxonomy( 'project-type', array( 'project' ), $args );

	unset( $args );
	unset( $labels );

	$labels = array(
		'name'              => _x( 'Category', 'taxonomy general name', 'orfeo' ),
		'singular_name'     => _x( 'Category', 'taxonomy singular name', 'orfeo' ),
		'search_items'      => __( 'Search Category', 'orfeo' ),
		'all_items'         => __( 'All Categories', 'orfeo' ),
		'parent_item'       => __( 'Parent Category', 'orfeo' ),
		'parent_item_colon' => __( 'Parent Category:', 'orfeo' ),
		'edit_item'         => __( 'Edit Category', 'orfeo' ),
		'update_item'       => __( 'Update Category', 'orfeo' ),
		'add_new_item'      => __( 'Add New Category', 'orfeo' ),
		'new_item_name'     => __( 'New Type Category', 'orfeo' ),
		'menu_name'         => __( 'Category', 'orfeo' ),
	);

	$args = array(
		'hierarchical'      => true,
		'labels'            => $labels,
		'show_ui'           => true,
		'show_admin_column' => true,
		'query_var'         => true,
		'rewrite'           => array( 'slug' => 'category' ),
	);

	register_taxonomy( 'project-category', array( 'project' ), $args );

	unset( $args );
	unset( $labels );

	$labels = array(
		'name'              => _x( 'Localisation', 'taxonomy general name', 'orfeo' ),
		'singular_name'     => _x( 'Localisation', 'taxonomy singular name', 'orfeo' ),
		'search_items'      => __( 'Search Category', 'orfeo' ),
		'all_items'         => __( 'All Localisations', 'orfeo' ),
		'parent_item'       => __( 'Parent Localisation', 'orfeo' ),
		'parent_item_colon' => __( 'Parent Localisation:', 'orfeo' ),
		'edit_item'         => __( 'Edit Localisation', 'orfeo' ),
		'update_item'       => __( 'Update Localisation', 'orfeo' ),
		'add_new_item'      => __( 'Add New Localisation', 'orfeo' ),
		'new_item_name'     => __( 'New Type Localisation', 'orfeo' ),
		'menu_name'         => __( 'Localisation', 'orfeo' ),
	);

	$args = array(
		'hierarchical'      => true,
		'labels'            => $labels,
		'show_ui'           => true,
		'show_admin_column' => true,
		'query_var'         => true,
		'rewrite'           => array( 'slug' => 'localisation' ),
	);

	register_taxonomy( 'project-location', array( 'project' ), $args );

	unset( $args );
	unset( $labels );

	$labels = array(
		'name'              => _x( 'Labels and Certifications', 'taxonomy general name', 'orfeo' ),
		'singular_name'     => _x( 'Labels and Certifications', 'taxonomy singular name', 'orfeo' ),
		'search_items'      => __( 'Search Labels and Certifications', 'orfeo' ),
		'all_items'         => __( 'All Labels and Certifications', 'orfeo' ),
		'parent_item'       => __( 'Parent Labels and Certifications', 'orfeo' ),
		'parent_item_colon' => __( 'Parent Labels and Certifications:', 'orfeo' ),
		'edit_item'         => __( 'Edit Labels and Certifications', 'orfeo' ),
		'update_item'       => __( 'Update Labels and Certifications', 'orfeo' ),
		'add_new_item'      => __( 'Add New Label and Certification', 'orfeo' ),
		'new_item_name'     => __( 'New Type LocaLabel and Certificationlisation', 'orfeo' ),
		'menu_name'         => __( 'Label and Certification', 'orfeo' ),
	);

	$args = array(
		'hierarchical'      => true,
		'labels'            => $labels,
		'show_ui'           => true,
		'show_admin_column' => true,
		'query_var'         => true,
		'public' => false,
		'rewrite'           => array( 'slug' => 'certification' ),
	);

	register_taxonomy( 'project-certification', array( 'project' ), $args );

}

add_action( 'init', 'orfeo_create_project_taxonomies', 0 );