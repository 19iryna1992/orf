<?php

function collapse_flexible_content () {
	?>
		<script type="text/javascript">
			window.addEventListener('DOMContentLoaded', () => {
				const layout = document.querySelectorAll('.acf-flexible-content .layout:not(.acf-clone)');

				if (layout) {
					layout.forEach(item => {
						item.classList.add('-collapsed');
					})
				}
			});
		</script>
	<?php
}

add_action('acf/input/admin_head', 'collapse_flexible_content');