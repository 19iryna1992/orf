<?php
/**
 * Theme shortcodes
 *
 * @package    WordPress
 * @subpackage orfeo
 * @since      orfeo 1.0
 */

 function current_year_shortcode() {
    $year = date('Y');
    return $year;
}
add_shortcode( 'current_year', 'current_year_shortcode' );
